/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>

#include <desqui/DropDownPlugin.hpp>

class FilesPlugin : public QObject, public DesQ::Plugin::DropDown {
    Q_OBJECT;

    Q_PLUGIN_METADATA( IID "org.DesQ.Plugin.DropDown" );
    Q_INTERFACES( DesQ::Plugin::DropDown );

    public:
        /* Name of the plugin */
        QString name();

        /* Icon for the plugin */
        QIcon icon();

        /* The plugin version */
        QString version();

        /* The files widget */
        QWidget *widget( QWidget * );
};
