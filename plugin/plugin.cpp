/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "plugin.hpp"
#include "DesQFiles.hpp"
#include <desq/Utils.hpp>

DFL::Settings                *filesSett = nullptr;
DFL::Storage::Manager        *diskMgr   = nullptr;
DFL::XDG::ApplicationManager *appsMgr   = nullptr;

int SideViewWidth = 200;

/* Name of the plugin */
QString FilesPlugin::name() {
    return "DesQ Files";
}


/* Name of the plugin */
QIcon FilesPlugin::icon() {
    return QIcon::fromTheme( "desq-files" );
}


/* The plugin version */
QString FilesPlugin::version() {
    return QString( PROJECT_VERSION );
}


/* The files plugin widget */
QWidget * FilesPlugin::widget( QWidget *parent ) {
    filesSett = DesQ::Utils::initializeDesQSettings( "Files", "Files" );
    diskMgr   = new DFL::Storage::Manager();
    appsMgr   = new DFL::XDG::ApplicationManager();

    return new DesQ::Files::UI( QDir::homePath(), parent );
}
