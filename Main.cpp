/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <signal.h>

#include "DesQFiles.hpp"
#include <desq/Utils.hpp>

#include <DFApplication.hpp>
#include <DFUtils.hpp>
#include <DFXdg.hpp>

#include <QThreadPool>

DFL::Settings                *filesSett = nullptr;
DFL::Storage::Manager        *diskMgr   = nullptr;
DFL::XDG::ApplicationManager *appsMgr   = nullptr;
int SideViewWidth = 200;

QString getDefaultPath() {
    QString path;

    if ( filesSett->value( "OpenLastUsed" ) ) {
        path = ( QString )filesSett->value( "Session::LastUsed" );

        /* If path is empty, open with StartupPath */
        if ( path.isEmpty() ) {
            path = ( QString )filesSett->value( "StartupPath" );
        }
    }

    /* Open with a startup path */
    else {
        path = ( QString )filesSett->value( "StartupPath" );

        if ( path.startsWith( "$HOME" ) ) {
            path.replace( 0, 5, QDir::homePath() );
        }

        else if ( path.startsWith( "~" ) ) {
            path.replace( 0, 1, QDir::homePath() );
        }
    }

    return path;
}


int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Files.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    DFL::SHOW_INFO_ON_CONSOLE  = true;
    DFL::SHOW_DEBUG_ON_CONSOLE = true;

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Files started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application *app = new DFL::Application( argc, argv );

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Files" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setDesktopFileName( "desq-files" );

    app->interceptSignal( SIGSEGV, true );
    app->interceptSignal( SIGINT,  true );
    app->interceptSignal( SIGQUIT, true );
    app->interceptSignal( SIGTERM, true );
    app->interceptSignal( SIGABRT, true );

    if ( app->lockApplication() ) {
        filesSett = DesQ::Utils::initializeDesQSettings( "Files", "Files" );
        diskMgr   = new DFL::Storage::Manager();
        appsMgr   = new DFL::XDG::ApplicationManager();

        QThreadPool::globalInstance()->start(
            [ = ] () {
                appsMgr->parseDesktops();
            }
        );

        DesQ::Files::UI *files;

        if ( argc == 1 ) {
            files = new DesQ::Files::UI( getDefaultPath() );
        }

        else {
            files = new DesQ::Files::UI( argv[ 1 ] );
        }

        QObject::connect( app, &DFL::Application::messageFromClient, files, &DesQ::Files::UI::receiveMessage );

        if ( filesSett->value( "Session::Maximized" ) ) {
            files->showMaximized();
        }

        else {
            files->show();
        }

        return app->exec();
    }

    else {
        /** Just run the app */
        if ( argc == 1 ) {
            /* If OpenLastUsed is true, use it */
            app->messageServer( QString( "new-window\n%1" ).arg( getDefaultPath() ) );
        }

        /** Open a specific dir(s) */
        else {
            for ( int i = 1; i < argc; i++ ) {
                app->messageServer( QString( "new-window\n%1" ).arg( argv[ i ] ) );
            }
        }

        return 0;
    }

    return 0;
}
