# DesQ Files
## A simple file manager for the DesQ Desktop Environment

This is a app to browse the file system. It is built to run smoothly on devices with different form factors.
Some of its features are

* Fast startup and browsing
* File Association support
* Full drag and drop support
* Auto update devices list
* Custom Folder Icons and Thumbnail Preview
* Complete mime-icon support
* SingleApplication Mode for resource sharing


### Dependencies
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* libdesq (https://gitlab.com/DesQ/libdesq)
* libdesqui (https://gitlab.com/DesQ/libdesqui)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQApps/DesQFiles.git DesQFiles`
- Enter the `DesQFiles` folder
  * `cd DesQFiles`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Upcoming
* Any other feature you request for... :)
