/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <DFUtils.hpp>
#include <DFXdg.hpp>

#include <desq/Utils.hpp>

#include <desqui/ActionBar.hpp>
#include <desqui/ActionButton.hpp>

// Local Headers
#include "TrashManager.hpp"
#include "DesQFiles.hpp"
#include "TrashView.hpp"
#include "ViewWidgets.hpp"

DesQ::Files::UI::UI( QString path, QWidget *parent ) : DesQUI::MainWindow( "Files", parent ) {
    createUI();
    setWindowProperties();

    if ( path.endsWith( "/" ) ) {
        path.chop( 1 );
    }

    changeDir( path );
    view->setFocus();

    setAttribute( Qt::WA_TranslucentBackground );
}


DesQ::Files::UI::~UI() {
    delete fsm;
    delete view;
    delete sideView;
    delete viewDlg;
    delete bar;
    delete path;
    delete magicBtn;
}


void DesQ::Files::UI::createUI() {
    /** Action and Path Bar */
    path = new DesQ::Files::PathBar( this );
    bar  = new DesQUI::ActionBar( this );

    QGridLayout *barLyt = new QGridLayout();

    barLyt->setContentsMargins( QMargins() );
    barLyt->setSpacing( 0 );

    barLyt->addWidget( path, 0, 0, Qt::AlignVCenter | Qt::AlignRight );
    barLyt->addWidget( bar,  0, 0, Qt::AlignVCenter | Qt::AlignLeft );

    /** Side and Main Views */
    view = new DesQ::Files::View( this );
    fsm  = new DesQ::Files::Model( this );

    view->setModel( fsm );

    sideView = new DesQ::Files::SideView( this );
    magicBtn = new DesQUI::ActionButton( "Actions", QIcon::fromTheme( "desq" ), "" );

    connect(
        view, &DesQ::Files::View::load, [ = ] ( QString pth ) {
            qDebug() << pth;
            changeDir( pth );
        }
    );

    connect(
        sideView, &DesQ::Files::SideView::load, [ = ] ( QString pth ) {
            changeDir( pth );
        }
    );

    QGridLayout *viewLyt = new QGridLayout();

    viewLyt->setContentsMargins( QMargins() );
    viewLyt->setSpacing( 0 );

    viewLyt->addWidget( sideView, 0, 0 );
    viewLyt->addWidget( view,     0, 1 );
    viewLyt->addWidget( magicBtn, 0, 1, Qt::AlignRight | Qt::AlignBottom );

    /** ViewBase */
    QVBoxLayout *baseLyt = new QVBoxLayout();
    baseLyt->setContentsMargins( QMargins() );
    baseLyt->setSpacing( 0 );

    baseLyt->addLayout( barLyt );
    baseLyt->addLayout( viewLyt );

    QWidget *base = new QWidget();
    base->setLayout( baseLyt );

    setMainView( base );

    /** Dialog */
    viewDlg = new DesQ::Files::Dialog( this );
    viewDlg->hide();

    /* Build the SideBar */
    buildActions();
    buildShortcuts();
    bar->setFixed( true );
}


void DesQ::Files::UI::receiveMessage( QString msg, int ) {
    handleAction( msg );
}


void DesQ::Files::UI::setWindowProperties() {
    setAppTitle( "DesQ Files" );
    setAppIcon( QIcon( ":/icons/desq-files.svg" ) );

    setMinimumSize( 800, 600 );

    setGeometry( filesSett->value( "Session::Geometry" ) );
}


void DesQ::Files::UI::goHome() {
    view->setRootIndex( fsm->setRootPath( QDir::homePath() ) );
}


void DesQ::Files::UI::changeDir( const QModelIndex& idx, bool newWin ) {
    changeDir( fsm->filePath( idx ), newWin );
}


void DesQ::Files::UI::changeDir( QString pth, bool newWin ) {
    QFileInfo info( pth );

    if ( info.isDir() ) {
        if ( newWin ) {
            DesQ::Files::UI *files = new DesQ::Files::UI( pth );
            files->show();

            return;
        }

        else {
            if ( info.exists() ) {
                path->setPath( info.filePath() );
                view->setRootIndex( fsm->setRootPath( pth ) );
                sideView->highlight( pth );
            }

            else {
                QMessageBox::information(
                    this,
                    "DesQ Files | Invalid path",
                    "The location you are trying to load does not exist. " \
                    "<br><b><tt>" + pth + "</tt></b><br>"
                    "Please check the path and try again."
                );
            }
        }

        return;
    }

    else if ( info.isFile() ) {
        QString mimeType = DFL::Utils::getMimeType( pth );
        qDebug() << pth << mimeType;

        DFL::XDG::ApplicationManager *appMgr = new DFL::XDG::ApplicationManager();
        QString defaultApp = appMgr->defaultAppForMimeType( mimeType );

        if ( not defaultApp.isEmpty() ) {
            DFL::XDG::DesktopFile desktop( defaultApp );
            qDebug() << "Starting default:" << defaultApp;
            bool ret = desktop.startApplication( { pth } );

            if ( ret ) {
                return;
            }
        }

        // Open this file - simplest way is use xdg-open
        qDebug() << "Try to open with xdg-open";
        bool ret = QProcess::startDetached( "xdg-open", { pth } );

        if ( ret ) {
            return;
        }
    }

    QMessageBox::information(
        this,
        "DesQ Files | Unable to open node",
        QString(
            "I have no way to open the node <tt><b>%1</b></tt>. "
            "Please reconsider what you're trying to do."
        ).arg( info.baseName() )
    );
}


void DesQ::Files::UI::focusInEvent( QFocusEvent *fEvent ) {
    QWidget::focusInEvent( fEvent );
    qApp->processEvents();

    view->setFocus();
}


void DesQ::Files::UI::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    if ( viewDlg and viewDlg->isVisible() ) {
        viewDlg->resize( rEvent->size() );
    }

    if ( rEvent->size().width() < 600 ) {
        sideView->setFixedWidth( 36 );
    }

    else {
        sideView->setFixedWidth( SideViewWidth );
    }
}


void DesQ::Files::UI::closeEvent( QCloseEvent *cEvent ) {
    filesSett->setValue( "Session::LastUsed", fsm->rootPath() );
    DesQUI::MainWindow::closeEvent( cEvent );
}


void DesQ::Files::UI::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    /** Draw the background only if we are the top-level window */
    if ( parent() == nullptr ) {
        painter.setPen( Qt::NoPen );
        painter.fillRect( geometry(), QColor( 0, 0, 0, 230 ) );
    }

    QPoint tl( bar->geometry().topLeft() );
    QPoint br( path->geometry().bottomRight() );

    painter.fillRect( QRect( tl, br ),      QColor( 0, 0, 0, 120 ) );
    painter.fillRect( sideView->geometry(), QColor( 0, 0, 0, 120 ) );

    painter.setPen( QColor( 30, 30, 30 ) );
    painter.drawLine( bar->geometry().bottomLeft(),    path->geometry().bottomRight() );
    painter.drawLine( sideView->geometry().topRight(), sideView->geometry().bottomRight() );

    painter.end();

    QMainWindow::paintEvent( pEvent );
}
