/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <DFUtils.hpp>
#include <DFXdg.hpp>

#include <desq/Utils.hpp>

#include <desqui/ActionBar.hpp>
#include <desqui/ActionButton.hpp>

#include <QThreadPool>

// Local Headers
#include "DesQFiles.hpp"
#include "ViewDialogs.hpp"
#include "TrashView.hpp"
#include "TrashModel.hpp"
#include "TrashManager.hpp"

void DesQ::Files::UI::buildActions() {
    /**
     * Context Info:
     * NoSel      - No selection
     * FileSel    - Single file selected
     * FilesSel   - One or more files selected
     * FolderSel  - Single folder selected
     * FoldersSel - One or more folders selected
     * OneSel     - One node is selected
     * SomeSel    - One or more node(s) is selected
     * AnySel     - Zero, one or more node(s) is selected
     */

    /** Single node selected: Open that node, internally open the folder, and externally open any other */
    magicBtn->addAction( "Open",      QIcon::fromTheme( "document-open" ),  "OneSel",     "open" );
    /** Single node selected: Open that node using the Open-with dialog */
    magicBtn->addAction( "Open With", QIcon( ":/icons/open-with.png" ),     "SomeSel",    "open-with" );
    /** One or more folder(s) selected: Provide options to open in new window/new tab/split view */
    magicBtn->addAction( "Open In",   QIcon( ":/icons/open-with.png" ),     "FoldersSel", "open-in" );
    /** One or more node(s) selected: Show the Copy to dialog */
    magicBtn->addAction( "Copy",      QIcon::fromTheme( "edit-copy" ),      "SomeSel",    "copy" );
    /** One or more node(s) selected: Show the Move to dialog */
    magicBtn->addAction( "Move",      QIcon::fromTheme( "edit-move" ),      "SomeSel",    "move" );
    /** One or mode node(s) selected: Show the share dialog */
    magicBtn->addAction( "Share",     QIcon::fromTheme( "document-share" ), "SomeSel",    "share" );
    /** One or mode node(s) selected: Send to trash */
    magicBtn->addAction( "Trash",     QIcon::fromTheme( "user-trash" ),     "SomeSel",    "delete-trash" );
    /** One or mode node(s) selected: Delete from disk */
    magicBtn->addAction( "Delete",    QIcon::fromTheme( "edit-delete" ),    "SomeSel",    "delete" );

    connect( magicBtn,               &DesQUI::ActionButton::triggered,       this, &DesQ::Files::UI::handleAction );

    /** Connect the changes in selection to contexts */
    connect( view->selectionModel(), &QItemSelectionModel::selectionChanged, this, &DesQ::Files::UI::updateContexts );

    bar->addPage();
    bar->addAction( 0, "New", QIcon::fromTheme( "folder-new" ), "#1", "Add new file/folder", false, false );
    bar->addSeparator( 0 );
    bar->addAction( 0, "Previews", QIcon::fromTheme( "view-preview" ), "toggle-previews", "Toggle previews", true, filesSett->value( "ShowThumb" ) );
    bar->addAction( 0, "Hidden",   QIcon::fromTheme( "view-hidden" ),  "toggle-hidden",   "Toggle hidden",   true, filesSett->value( "ShowHidden" ) );
    bar->addSeparator( 0 );
    bar->addAction( 0, "Terminal", QIcon::fromTheme( "utilities-terminal" ), "open-terminal", "Open terminal", false, false );
    bar->addSeparator( 0 );
    bar->addAction( 0, "Trash", QIcon::fromTheme( "user-trash" ), "trash://", "Show trash", false, false );
    bar->addSeparator( 0 );
    bar->addAction( 0, "Properties", QIcon::fromTheme( "document-properties" ), "properties", "Properties", false, false );

    bar->addPage();
    bar->addAction( 1, "New Folder", QIcon::fromTheme( "folder-new" ),   "new-folder;#0", "New Folder", false, false );
    bar->addAction( 1, "New File",   QIcon::fromTheme( "document-new" ), "new-file;#0",   "New File",   false, false );
    bar->addStretch( 1 );
    bar->addAction( 1, "Back", QIcon::fromTheme( "close" ), "#0", "Go back", false, false );

    connect( bar, &DesQUI::ActionBar::action, this, &DesQ::Files::UI::handleAction );
    connect(
        view, &DesQ::Files::View::activated, [ = ]( const QModelIndex& idx ) {
            changeDir( idx );
        }
    );
}


void DesQ::Files::UI::buildShortcuts() {
    QAction *actCopy = new QAction( QIcon(), "Copy", this );

    actCopy->setShortcut( QKeySequence( Qt::CTRL | Qt::Key_C ) );
    connect(
        actCopy, &QAction::triggered, [ = ]( ) {
            handleAction( "copy" );
        }
    );

    QAction *actMove = new QAction( QIcon(), "Move", this );

    actMove->setShortcut( QKeySequence( Qt::CTRL | Qt::Key_X ) );
    connect(
        actMove, &QAction::triggered, [ = ]( ) {
            handleAction( "move" );
        }
    );

    QAction *actPaste = new QAction( QIcon(), "Paste", this );

    actPaste->setShortcut( QKeySequence( Qt::CTRL | Qt::Key_X ) );
    connect(
        actPaste, &QAction::triggered, [ = ]( ) {
            handleAction( "paste" );
        }
    );

    // QAction *actShare = new QAction( QIcon(), "Share", this );
    // actShare->setShortcut( QKeySequence( Qt:: | Qt::Key_ ) );
    // connect(
    // actShare, &QAction::triggered, [=]( ) {
    // handleAction( "share" );
    // }
    // );

    QAction *actTrash = new QAction( QIcon(), "Trash", this );

    actTrash->setShortcut( QKeySequence( Qt::Key_Delete ) );
    connect(
        actTrash, &QAction::triggered, [ = ]( ) {
            handleAction( "delete-trash" );
        }
    );

    QAction *actDelete = new QAction( QIcon(), "Delete", this );

    actDelete->setShortcut( QKeySequence( Qt::SHIFT | Qt::Key_Delete ) );
    connect(
        actDelete, &QAction::triggered, [ = ]( ) {
            handleAction( "delete" );
        }
    );

    QAction *actNew = new QAction( QIcon(), "New", this );

    actNew->setShortcut( QKeySequence( Qt::Key_F10 ) );
    connect(
        actNew, &QAction::triggered, [ = ]( ) {
            handleAction( "folder-new" );
        }
    );

    QAction *actPreviews = new QAction( QIcon(), "Toggle Previews", this );

    actPreviews->setShortcut( QKeySequence( Qt::CTRL | Qt::SHIFT | Qt::Key_P ) );
    connect(
        actPreviews, &QAction::triggered, [ = ]( ) {
            handleAction( "toggle-previews" );
        }
    );

    QAction *actHidden = new QAction( QIcon(), "Hidden", this );

    actHidden->setShortcut( QKeySequence( Qt::ALT | Qt::Key_Period ) );
    connect(
        actHidden, &QAction::triggered, [ = ]( ) {
            handleAction( "toggle-hidden" );
        }
    );

    QAction *actTerminal = new QAction( QIcon(), "Terminal", this );

    actTerminal->setShortcut( QKeySequence( Qt::Key_F4 ) );
    connect(
        actTerminal, &QAction::triggered, [ = ]( ) {
            handleAction( "open-terminal" );
        }
    );

    QAction *actProperties = new QAction( QIcon(), "Properties", this );

    actProperties->setShortcut( QKeySequence( Qt::ALT | Qt::Key_Return ) );
    connect(
        actProperties, &QAction::triggered, [ = ]( ) {
            handleAction( "properties" );
        }
    );

    QMainWindow::addAction( actCopy );
    QMainWindow::addAction( actMove );
    QMainWindow::addAction( actPaste );
    QMainWindow::addAction( actTrash );
    QMainWindow::addAction( actDelete );
    QMainWindow::addAction( actNew );
    QMainWindow::addAction( actPreviews );
    QMainWindow::addAction( actHidden );
    QMainWindow::addAction( actTerminal );
    QMainWindow::addAction( actProperties );
}


void DesQ::Files::UI::updateContexts( const QItemSelection& selection, const QItemSelection& ) {
    /** We're bothered about selected nodes only, and not deselected nodes */

    int files   = 0;
    int folders = 0;
    int others  = 0;

    /**
     * Context Info:
     * FileSel    - Single file selected
     * FilesSel   - One or more files selected
     * FolderSel  - Single folder selected
     * FoldersSel - One or more folders selected
     * OneSel     - One node is selected
     * SomeSel    - One or more node(s) is selected
     */

    for ( QModelIndex idx: selection.indexes() ) {
        QFileInfo info( fsm->fileInfo( idx ) );

        if ( info.isFile() ) {
            files++;
        }

        else if ( info.isDir() ) {
            folders++;
        }

        else {
            others++;
        }
    }

    QStringList contexts;

    if ( files == 1 ) {
        contexts << "FileSel" << "FilesSel" << "OneSel" << "SomeSel";
    }

    else if ( folders == 1 ) {
        contexts << "FolderSel" << "FoldersSel" << "OneSel" << "SomeSel";
    }

    else if ( files > 1 ) {
        contexts << "FilesSel" << "SomeSel";
    }

    else if ( folders > 1 ) {
        contexts << "FoldersSel" << "SomeSel";
    }

    else if ( others == 1 ) {
        contexts << "OneSel";
    }

    else if ( others > 1 ) {
        contexts << "SomeSel";
    }

    else if ( files and folders ) {
        contexts << "SomeSel";
    }

    else if ( files and others ) {
        contexts << "SomeSel";
    }

    else if ( folders and others ) {
        contexts << "SomeSel";
    }

    else if ( files and folders and others ) {
        contexts << "SomeSel";
    }

    // qDebug() << contexts.join( ";" ) << files << folders << others;
    magicBtn->setContexts( contexts.join( ";" ) );
}


void DesQ::Files::UI::handleAction( QString action ) {
    if ( action == "open-terminal" ) {
        // Open a terminal
        QProcess::startDetached( "desq-term", { "--workdir", fsm->rootPath() } );
        return;
    }

    else if ( action.startsWith( "/" ) ) {
        // Change to this path
        if ( QFile::exists( action ) ) {
            changeDir( action );
        }

        else {
            qDebug() << action << "does not exist";
        }

        return;
    }

    else if ( action.startsWith( "trash://" ) ) {
        // Show trash
        TrashView *tView = new TrashView();
        tView->show();

        return;
    }

    else if ( action.startsWith( "delete-trash" ) ) {
        showTrashDialog();
    }

    else if ( action.startsWith( "delete" ) ) {
        showDeleteDialog();
    }

    else if ( action.startsWith( "new-window\n" ) ) {
        QStringList paths = action.split( "\n", Qt::SkipEmptyParts );
        paths.takeFirst();

        DesQ::Files::UI *fm;
        for ( QString path: paths ) {
            if ( not QFile::exists( path ) ) {
                continue;
            }

            fm = new DesQ::Files::UI( path );

            if ( filesSett->value( "Session::Maximized" ) ) {
                fm->showMaximized();
            }

            else {
                fm->show();
            }
        }

        return;
    }

    else if ( action == "toggle-previews" ) {
        filesSett->setValue( "ShowThumb", not filesSett->value( "ShowThumb" ) );
        fsm->reload();
    }

    else if ( action == "toggle-hidden" ) {
        bool hidden = filesSett->value( "ShowHidden" );
        qDebug() << (hidden ? "Hiding dot-files." : "Showing dot-files");

        filesSett->setValue( "ShowHidden", not hidden );
        fsm->setShowHidden( not hidden );
    }

    else if ( action == "open-with" ) {
        showOpenWith();
    }

    else if ( action == "properties" ) {
        // Properties dialog
        qDebug() << "Comping up shortly";
    }

    else {
        //
        qDebug() << "Unknown action" << action;
        return;
    }
}


void DesQ::Files::UI::showOpenWith() {
    viewDlg->setTitle( "<b>Open with...</b>" );
    viewDlg->setDescription( "Choose an application to open the file <tt><b>trial.cpp</b></tt>." );
    QCheckBox *cb = new QCheckBox( "Remember my choice." );

    viewDlg->setExtraWidget( cb );
    viewDlg->setEditInfo( "&Command:" );
    viewDlg->setPlaceholderText( "Type your custom command here..." );

    QStandardItemModel *appsModel = new QStandardItemModel();

    viewDlg->setModel( appsModel );

    viewDlg->show();
    viewDlg->setFocus();

    /** Force show */
    qApp->processEvents();

    /** Selected nodes: will help in model loading too */
    QModelIndexList selection = view->selectionModel()->selectedIndexes();

    QThreadPool::globalInstance()->start(
        [ = ] () {
            QFont bold( font() );
            bold.setWeight( QFont::Bold );

            /** Pointer object that will be used everywhere */
            QStandardItem *item;

            /** "Preferred Applications" title item */
            item = new QStandardItem();
            item->setText( "Preferred Applications" );
            item->setFlags( Qt::NoItemFlags );
            item->setData( bold, Qt::FontRole );
            appsModel->appendRow( item );
            qApp->processEvents();

            /** 1st Index mime type */
            QString file = fsm->filePath( selection.at( 0 ) );

            QString mimeName = DFL::Utils::getMimeType( file );
            for ( QString app: appsMgr->appsForMimeType( mimeName ) ) {
                DFL::XDG::DesktopFile desktop( app );

                if ( not desktop.isValid() or not desktop.visible() ) {
                    continue;
                }

                item = new QStandardItem();

                item->setIcon( QIcon::fromTheme( desktop.icon() ) );
                item->setText( desktop.name() );

                item->setData( desktop.desktopName(),    Qt::UserRole + 1 );
                item->setData( desktop.desktopFileUrl(), Qt::UserRole + 2 );

                item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );

                appsModel->appendRow( item );
            }

            /** Also include apps meant for mime-type ancestors */
            QMimeType mimeType = mimeDb.mimeTypeForName( mimeName );
            for ( QString mimeName: mimeType.allAncestors() ) {
                for ( QString app: appsMgr->appsForMimeType( mimeName ) ) {
                    DFL::XDG::DesktopFile desktop( app );

                    if ( not desktop.isValid() or not desktop.visible() ) {
                        continue;
                    }

                    item = new QStandardItem();

                    item->setIcon( QIcon::fromTheme( desktop.icon() ) );
                    item->setText( desktop.name() );

                    item->setData( desktop.desktopName(),    Qt::UserRole + 1 );
                    item->setData( desktop.desktopFileUrl(), Qt::UserRole + 2 );

                    item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );

                    appsModel->appendRow( item );
                }
            }

            /** Spacer item */
            item = new QStandardItem();
            item->setText( "  " );
            item->setFlags( Qt::NoItemFlags );
            item->setData( bold, Qt::FontRole );
            appsModel->appendRow( item );

            /** "All Applications" title item */
            item = new QStandardItem();
            item->setText( "All Applications" );
            item->setFlags( Qt::NoItemFlags );
            item->setData( bold, Qt::FontRole );
            appsModel->appendRow( item );
            qApp->processEvents();

            for ( DFL::XDG::DesktopFile desktop: appsMgr->allApplications() ) {
                if ( not desktop.visible() ) {
                    continue;
                }

                item = new QStandardItem();

                item->setIcon( QIcon::fromTheme( desktop.icon() ) );
                item->setText( desktop.name() );

                item->setData( desktop.desktopName(),    Qt::UserRole + 1 );
                item->setData( desktop.desktopFileUrl(), Qt::UserRole + 2 );

                item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );

                appsModel->appendRow( item );
                qApp->processEvents();
            }
        }
    );

    if ( viewDlg->exec() ) {
        QModelIndexList apps = viewDlg->selectedIndexes();

        QStringList nodes;
        for ( QModelIndex idx: selection ) {
            nodes << fsm->filePath( idx );
        }

        DFL::XDG::DesktopFile desktop( apps.at( 0 ).data( Qt::UserRole + 2 ).toString() );

        if ( desktop.multipleArgs() ) {
            desktop.startApplication( nodes );
        }

        else {
            // WE SHOULD BE WARNING THE USER ABOUT THIS
            for ( QString node: nodes ) {
                desktop.startApplication( { node } );
            }
        }

        /** If this app is to be made default for this mimetype */
        if ( cb->isChecked() ) {
            appsMgr->setDefaultAppForMimeType( DFL::Utils::getMimeType( nodes.at( 0 ) ), desktop.desktopName() );
        }
    }
}


void DesQ::Files::UI::showFileFolderDialog( int ) {
}


void DesQ::Files::UI::showDeleteDialog() {
    QModelIndexList selection = view->selectionModel()->selectedIndexes();

    viewDlg->setTitle( "<b>Delete files...</b>" );
    viewDlg->setDescription( "Do you want to delete the following items permanently?" );
    viewDlg->showLineEdit( false );

    QStandardItemModel *itemsModel = new QStandardItemModel();

    viewDlg->setModel( itemsModel );

    viewDlg->show();
    viewDlg->setFocus();

    /** Force show */
    qApp->processEvents();

    /** Pointer object that will be used everywhere */
    QStandardItem *item;

    QStringList selectedFiles;

    for ( QModelIndex idx: selection ) {
        selectedFiles << fsm->filePath( idx );
        item = new QStandardItem();

        item->setText( idx.data( Qt::DisplayRole ).toString() );
        item->setIcon( idx.data( Qt::DecorationRole ).value<QIcon>() );
        item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable );
        item->setCheckable( true );
        item->setCheckState( Qt::Checked );

        itemsModel->appendRow( item );
    }

    int ret = viewDlg->exec();

    if ( ret ) {
        TrashManager::deleteFileTotally( selectedFiles, false );
    }
}


void DesQ::Files::UI::showTrashDialog() {
    QModelIndexList selection = view->selectionModel()->selectedIndexes();

    viewDlg->setTitle( "<b>Trash files..?</b>" );
    viewDlg->setDescription( "Do you want to move these items to trash?" );
    viewDlg->showLineEdit( false );

    QStandardItemModel *itemsModel = new QStandardItemModel();

    viewDlg->setModel( itemsModel );

    viewDlg->show();
    viewDlg->setFocus();

    /** Force show */
    qApp->processEvents();

    /** Pointer object that will be used everywhere */
    QStandardItem *item;

    QStringList selectedFiles;

    for ( QModelIndex idx: selection ) {
        selectedFiles << fsm->filePath( idx );
        item = new QStandardItem();

        item->setText( idx.data( Qt::DisplayRole ).toString() );
        item->setIcon( idx.data( Qt::DecorationRole ).value<QIcon>() );
        item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable );
        item->setCheckable( true );
        item->setCheckState( Qt::Checked );

        itemsModel->appendRow( item );
    }

    int ret = viewDlg->exec();

    if ( ret ) {
        TrashManager::moveToTrash( selectedFiles );
    }
}
