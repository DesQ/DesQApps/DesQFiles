/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

// Local Headers
#include <desqui/MainWindow.hpp>
#include "View.hpp"
#include "Model.hpp"
#include "ViewDialogs.hpp"

namespace DesQ {
    namespace Files {
        class UI;
        class PathBar;
        class SideView;
    }
}

namespace DesQUI {
    class ActionBar;
    class ActionButton;
}

class DesQ::Files::UI : public DesQUI::MainWindow {
    Q_OBJECT;

    public:
        UI( QString path = QDir::homePath(), QWidget *parent = nullptr );
        ~UI();

    private:
        /* Variables */
        DesQ::Files::Model *fsm         = nullptr;
        DesQ::Files::View *view         = nullptr;
        DesQ::Files::SideView *sideView = nullptr;

        DesQ::Files::Dialog *viewDlg = nullptr;

        DesQUI::ActionBar *bar         = nullptr;
        DesQ::Files::PathBar *path     = nullptr;
        DesQUI::ActionButton *magicBtn = nullptr;

        void createUI();
        void setWindowProperties();

        void buildActions();
        void buildShortcuts();
        void updateContexts( const QItemSelection&, const QItemSelection& );

    public Q_SLOTS:
        void receiveMessage( QString, int );

    /* Slots */
    private Q_SLOTS:
        /* Load home directory */
        void goHome();

        /* Change to directory pointed by @idx (optionally open in new window)  */
        void changeDir( const QModelIndex& idx, bool newWin = false );

        /* Change to directory pointed by @path (optionally open in new window)  */
        void changeDir( QString path, bool newWin = false );

        /* Handle the various actions emitted from DesQActionBar */
        void handleAction( QString path );

        /**
         * Below functions are various action helpers
         */

        /** To show the open-with dialog */
        void showOpenWith();

        /** To show the delete dialog */
        void showDeleteDialog();

        /** To show the trash dialog */
        void showTrashDialog();

        /** To show new file/folder dialog */
        void showFileFolderDialog( int type );

    protected:
        void focusInEvent( QFocusEvent * );

        void resizeEvent( QResizeEvent *rEvent );
        void closeEvent( QCloseEvent *cEvent );

        void paintEvent( QPaintEvent *pEvent );
};
