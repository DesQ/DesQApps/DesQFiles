/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include <desqui/UI.hpp>

#include "ViewWidgets.hpp"


QWidget * Separator::vertical( QWidget *parent ) {
    return new Separator( Separator::Vertical, parent );
}


QWidget * Separator::horizontal( QWidget *parent ) {
    return new Separator( Separator::Horizontal, parent );
}


Separator::Separator( Separator::Mode mode, QWidget *parent ) : QWidget( parent ) {
    mMode = mode;

    switch ( mode ) {
        case Separator::Horizontal: {
            setContentsMargins( QMargins() );
            setFixedHeight( 1 );

            gradient = QLinearGradient( QPoint( 0, 0 ), QPoint( 2000, 0 ) );
            gradient.setColorAt( 0.0, Qt::transparent );
            gradient.setColorAt( 0.4, palette().color( QPalette::Base ) );
            gradient.setColorAt( 0.6, palette().color( QPalette::Base ) );
            gradient.setColorAt( 1.0, Qt::transparent );

            break;
        };

        case Separator::Vertical: {
            setContentsMargins( QMargins() );
            setFixedWidth( 1 );

            gradient = QLinearGradient( QPoint( 0, 0 ), QPoint( 0, 2000 ) );
            gradient.setColorAt( 0.0, Qt::transparent );
            gradient.setColorAt( 0.4, palette().color( QPalette::Base ) );
            gradient.setColorAt( 0.6, palette().color( QPalette::Base ) );
            gradient.setColorAt( 1.0, Qt::transparent );

            break;
        }
    }
}


void Separator::resizeEvent( QResizeEvent *rEvent ) {
    QWidget::resizeEvent( rEvent );
    rEvent->accept();

    switch ( mMode ) {
        case Separator::Horizontal: {
            gradient.setFinalStop( rEvent->size().width(), 0 );
            break;
        }

        case Separator::Vertical: {
            gradient.setFinalStop( 0, rEvent->size().height() );
            break;
        }
    }

    repaint();
}


void Separator::paintEvent( QPaintEvent *pEvent ) {
    QPainter *painter = new QPainter( this );

    painter->setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing );

    switch ( mMode ) {
        case Separator::Horizontal: {
            painter->setPen( QPen( QBrush( gradient ), 1.0 ) );
            painter->drawLine( rect().topLeft(), rect().topRight() );
            break;
        }

        case Separator::Vertical: {
            painter->setPen( QPen( QBrush( gradient ), 1.0 ) );
            painter->drawLine( rect().topLeft(), rect().bottomLeft() );
            break;
        }
    }

    painter->end();

    pEvent->accept();
}


ActionButton::ActionButton( QWidget *parent ) : QAbstractButton( parent ) {
    setAttribute( Qt::WA_TranslucentBackground );

    /** Default actions */
    connect(
        this, &QAbstractButton::clicked, [ = ] () {
            if ( isCheckable() ) {
                setChecked( !isChecked() );
            }

            if ( mAutoClose ) {
                parent->close();
            }
        }
    );

    /** So that we'll repaint when mouse enters */
    setMouseTracking( true );
}


bool ActionButton::autoClose() {
    return mAutoClose;
}


void ActionButton::setAutoClose( bool yes ) {
    mAutoClose = yes;

    if ( mAutoClose ) {
        setCheckable( false );
        setChecked( false );
    }

    else {
        setCheckable( true );
    }
}


void ActionButton::setIcon( QIcon icon ) {
    QAbstractButton::setIcon( icon );

    mIcon = icon.pixmap( 64 ).scaled( iconSize(), Qt::KeepAspectRatio, Qt::SmoothTransformation );
    repaint();
}


void ActionButton::setIconSize( QSize size ) {
    QAbstractButton::setIconSize( size );

    mIcon = icon().pixmap( 64 ).scaled( size, Qt::KeepAspectRatio, Qt::SmoothTransformation );
    repaint();
}


void ActionButton::enterEvent( QMouseEnterEvent *event ) {
    update();
    QAbstractButton::enterEvent( event );
}


void ActionButton::leaveEvent( QEvent *event ) {
    update();
    QAbstractButton::leaveEvent( event );
}


void ActionButton::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    QColor pen   = QColor( Qt::black );
    QColor brush = QColor( Qt::black );

    brush.setAlphaF( 0.3 );

    if ( underMouse() || mInside ) {
        pen = palette().color( QPalette::Highlight );

        if ( isDown() ) {
            brush = palette().color( QPalette::Highlight );
            brush.setAlphaF( 0.7 );
        }
    }

    else if ( isChecked() ) {
        pen   = palette().color( QPalette::Highlight );
        brush = palette().color( QPalette::Highlight );
        brush.setAlphaF( 0.7 );
    }

    else {
        pen   = palette().color( QPalette::Window );
        brush = palette().color( QPalette::Window );
    }

    painter.setPen( QPen( pen, 1.0 ) );
    painter.setBrush( brush );

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 5.0, 5.0 );

    painter.setPen( Qt::NoPen );
    painter.setBrush( Qt::NoBrush );

    qreal x = (width() - mIcon.width() ) / 2.0;
    qreal y = (height() - mIcon.height() ) / 2.0;

    painter.drawPixmap( QPointF( x, y ), mIcon );

    painter.end();

    pEvent->accept();
}


DesQNavButton::DesQNavButton( QWidget *parent ) : QWidget( parent ) {
    setContentsMargins( QMargins() );

    prevBtn = new QToolButton();
    prevBtn->setIcon( QIcon::fromTheme( "go-previous" ) );
    prevBtn->setFixedSize( QSize( 32, 32 ) );
    prevBtn->setIconSize( QSize( 24, 24 ) );
    prevBtn->setAutoRaise( true );
    prevBtn->setStyleSheet( "border: none;" );
    connect( prevBtn, &QToolButton::clicked, this, &DesQNavButton::back );

    upBtn = new QToolButton();
    upBtn->setIcon( QIcon::fromTheme( "go-up" ) );
    upBtn->setFixedSize( QSize( 32, 32 ) );
    upBtn->setIconSize( QSize( 24, 24 ) );
    upBtn->setAutoRaise( true );
    upBtn->setStyleSheet( "border: none;" );
    connect( upBtn, &QToolButton::clicked, this, &DesQNavButton::up );

    nextBtn = new QToolButton();
    nextBtn->setIcon( QIcon::fromTheme( "go-next" ) );
    nextBtn->setFixedSize( QSize( 32, 32 ) );
    nextBtn->setIconSize( QSize( 24, 24 ) );
    nextBtn->setAutoRaise( true );
    nextBtn->setStyleSheet( "border: none;" );
    connect( nextBtn, &QToolButton::clicked, this, &DesQNavButton::forward );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 1 );

    lyt->addWidget( prevBtn );
    lyt->addWidget( upBtn );
    lyt->addWidget( nextBtn );

    setLayout( lyt );

    setFixedSize( QSize( 98, 32 ) );

    prevCut = new QShortcut( QKeySequence( Qt::ALT | Qt::Key_Left ), this );
    connect( prevCut, &QShortcut::activated, this, &DesQNavButton::back );

    upCut = new QShortcut( QKeySequence( Qt::ALT | Qt::Key_Up ), this );
    connect( upCut,   &QShortcut::activated, this, &DesQNavButton::up );

    nextCut = new QShortcut( QKeySequence( Qt::ALT | Qt::Key_Right ), this );
    connect( nextCut, &QShortcut::activated, this, &DesQNavButton::forward );
}


void DesQNavButton::setBackEnabled( bool yes ) {
    prevBtn->setEnabled( yes );
    prevCut->setEnabled( yes );
}


void DesQNavButton::setForwardEnabled( bool yes ) {
    nextBtn->setEnabled( yes );
    nextBtn->setEnabled( yes );
}


void DesQNavButton::setUpEnabled( bool yes ) {
    upBtn->setEnabled( yes );
    upBtn->setEnabled( yes );
}


void DesQNavButton::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    QColor borderClr = palette().color( QPalette::Shadow );
    QColor bgClr     = palette().color( QPalette::Window );

    painter.save();
    painter.setPen( Qt::NoPen );

    if ( underMouse() ) {
        painter.setOpacity( 1.0 );
        painter.setBrush( bgClr );
        bgClr.setAlpha( 0.5 );
        painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
    }

    else {
        painter.setOpacity( 0.25 );
        painter.setBrush( Qt::transparent );
        painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
    }

    painter.restore();

    painter.save();
    painter.setPen( Qt::NoPen );

    if ( prevBtn->underMouse() ) {
        if ( prevBtn->isEnabled() ) {
            if ( prevBtn->isDown() ) {
                QLinearGradient grad( prevBtn->geometry().topRight(), prevBtn->geometry().topLeft() );
                grad.setColorAt( 0, QColor( 0, 180, 145, 30 ) );
                grad.setColorAt( 1, QColor( 0, 180, 145, 120 ) );
                painter.setBrush( grad );
            }

            else {
                painter.setBrush( QColor( 0, 180, 145, 63 ) );
            }
        }

        else {
            painter.setBrush( Qt::NoBrush );
        }

        painter.drawRoundedRect( prevBtn->geometry(), 3, 3 );
    }

    else if ( upBtn->underMouse() ) {
        if ( upBtn->isEnabled() ) {
            if ( upBtn->isDown() ) {
                QLinearGradient grad( upBtn->geometry().bottomLeft(), upBtn->geometry().topLeft() );
                grad.setColorAt( 0, QColor( 0, 180, 145, 30 ) );
                grad.setColorAt( 1, QColor( 0, 180, 145, 120 ) );
                painter.setBrush( grad );
            }

            else {
                painter.setBrush( QColor( 0, 180, 145, 63 ) );
            }
        }

        else {
            painter.setBrush( Qt::NoBrush );
        }

        painter.drawRoundedRect( upBtn->geometry(), 3, 3 );
    }

    else if ( nextBtn->underMouse() ) {
        if ( nextBtn->isEnabled() ) {
            if ( nextBtn->isDown() ) {
                QLinearGradient grad( nextBtn->geometry().topLeft(), nextBtn->geometry().topRight() );
                grad.setColorAt( 0, QColor( 0, 180, 145, 30 ) );
                grad.setColorAt( 1, QColor( 0, 180, 145, 120 ) );
                painter.setBrush( grad );
            }

            else {
                painter.setBrush( QColor( 0, 180, 145, 63 ) );
            }
        }

        else {
            painter.setBrush( Qt::NoBrush );
        }

        painter.drawRoundedRect( nextBtn->geometry(), 3, 3 );
    }

    else {
        painter.setBrush( Qt::NoBrush );
    }

    painter.restore();

    QWidget::paintEvent( pEvent );

    painter.save();
    painter.setRenderHint( QPainter::Antialiasing, false );
    painter.setPen( bgClr );
    painter.drawLine( QPoint( 31, 0 ), QPoint( 31, 31 ) );
    painter.drawLine( QPoint( 65, 0 ), QPoint( 65, 31 ) );
    painter.restore();

    painter.setPen( borderClr );
    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );

    painter.end();

    pEvent->accept();
}


DesQ::Files::PathBar::PathBar( QWidget *parent ) : QLabel( parent ) {
    setFixedHeight( DesQUI::ActionBarHeight );

    setAlignment( Qt::AlignVCenter | Qt::AlignRight );
    setContentsMargins( QMargins( 5, 0, 5, 0 ) );

    setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed ) );
}


void DesQ::Files::PathBar::setPath( QString path ) {
    if ( path == "/" ) {
        setText( "<b>FileSystem</b><br><small>/</small>" );
        return;
    }

    if ( path.endsWith( "/" ) ) {
        path.chop( 1 );
    }

    QFileInfo info( path );

    QString part1( info.baseName() );
    QString part2( info.path() );

    setText(
        QString(
            "<b>%1</b><br><small>%2</small>"
        ).arg( part1 ).arg( part2 )
    );
}
