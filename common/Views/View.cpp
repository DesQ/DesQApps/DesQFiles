/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include <DFXdg.hpp>

#include <DFStorage.hpp>

#include "IOProcesses.hpp"

#include "View.hpp"
#include "ViewDelegate.hpp"

#include "ViewWidgets.hpp"

QSize DesQ::Files::View::gridSize   = QSize( 256, 48 );
int   DesQ::Files::View::extraSpace = 10;

DesQ::Files::View::View( QWidget *parent ) : QListView( parent ) {
    setViewportMargins( 10, 10, 10, 10 );

    /* Navigation button */
    navBtn = new DesQNavButton( this );

    /* Context menu */
    menu = new DesQ::Files::ContextMenu( this );
    menu->hide();

    connect(
        navBtn, &DesQNavButton::back, [ = ] () {
            load( filesModel->previousFolder() );
        }
    );

    connect(
        navBtn, &DesQNavButton::forward, [ = ] () {
            load( filesModel->nextFolder() );
        }
    );

    connect(
        navBtn, &DesQNavButton::up, [ = ] () {
            load( filesModel->parentFolder() );
        }
    );

    setFrameStyle( QFrame::NoFrame );

    setViewMode( QListView::IconMode );
    setFlow( QListView::LeftToRight );

    setResizeMode( QListView::Adjust );
    setMovement( QListView::Static );

    setSelectionMode( QListView::ExtendedSelection );
    setSelectionBehavior( QAbstractItemView::SelectItems );

    setWrapping( true );
    setWordWrap( false );
    setTextElideMode( Qt::ElideRight );

    setUniformItemSizes( true );
    setAutoScroll( true );

    // setContextMenuPolicy( Qt::CustomContextMenu );

    setDragDropMode( QAbstractItemView::DragDrop );
    setDragEnabled( true );
    setAcceptDrops( true );

    QSize iSize = filesSett->value( "IconSize" );

    setIconSize( iSize );

    setItemDelegate( new DesQ::Files::ViewDelegate() );

    QPalette pltt( palette() );

    pltt.setColor( QPalette::Base, Qt::transparent );
    setPalette( pltt );
}


void DesQ::Files::View::setTileView() {
    setViewMode( QListView::ListMode );
    /* Recompute the gridSize */
    setIconSize( iconSize() );
}


void DesQ::Files::View::setIconView() {
    setViewMode( QListView::IconMode );
    /* Recompute the gridSize */
    setIconSize( iconSize() );
}


void DesQ::Files::View::setModel( QAbstractItemModel *fsm ) {
    filesModel = qobject_cast<DesQ::Files::Model *>( fsm );
    connect( filesModel, &DesQ::Files::Model::backEnabled,    navBtn, &DesQNavButton::setBackEnabled );
    connect( filesModel, &DesQ::Files::Model::forwardEnabled, navBtn, &DesQNavButton::setForwardEnabled );
    connect( filesModel, &DesQ::Files::Model::upEnabled,      navBtn, &DesQNavButton::setUpEnabled );


    QListView::setModel( fsm );
    connect( fsm, SIGNAL(updateItem(QModelIndex)), this, SLOT(updateItem(QModelIndex)) );
}


void DesQ::Files::View::setIconSize( const QSize& size ) {
    QListView::setIconSize( size );

    /* Icon View */
    if ( viewMode() == QListView::IconMode ) {
        int minWidth = size.width() + 30;
        int height   = size.height() + 30 + font().pointSize() * 2;

        /* Items per row */
        int items = ( int )(viewportWidth / (minWidth + mSpacing) );

        /* Minimum width of all items */
        int itemsWidth = items * minWidth;

        /* Empty space remaining */
        int empty = viewportWidth - itemsWidth;

        /* Extra space per item */
        int extra = empty / items;

        /* New grid size */
        gridSize   = QSize( minWidth + mSpacing + extra, height + mSpacing );
        extraSpace = mSpacing + extra;
        setSpacing( extraSpace );
    }

    /* Tile View */
    else {
        gridSize = QSize( 256 + 2, size.height() + 8 + 2 );
    }

    QListView::setGridSize( gridSize );
    setMinimumSize( 3 * gridSize );
}


void DesQ::Files::View::setGridSize( const QSize& ) {
    setIconSize( iconSize() );
}


QModelIndexList DesQ::Files::View::selectedIndexes() const {
    return QListView::selectedIndexes();
}


void DesQ::Files::View::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    /* To prevent crashes while hiding the split view */
    viewportWidth = qMax( viewport()->width(), 128 );
    setIconSize( iconSize() );

    // sideView->resize( 200, viewport()->height() );
    // sideView->move( 5, viewport()->geometry().top() );

    if ( navBtn ) {
        QRectF viewGeom( viewport()->geometry() );
        navBtn->move( viewGeom.left() + 5, viewGeom.bottom() - navBtn->height() - 5 );
    }
}


void DesQ::Files::View::contextMenuEvent( QContextMenuEvent *cEvent ) {
    menu->setSelection( selectedIndexes() );

    /**
     * When Qt::Popup flag is set use QContextMenuEvent::globalPos()
     * If Qt::Popup is not set, then use QContextMenuEvent::pos()
     */
    menu->move( cEvent->pos() );
    menu->show();

    menu->setFocus();

    cEvent->accept();
}


void DesQ::Files::View::updateItem( QModelIndex idx ) {
    QAbstractItemView::dataChanged( idx, idx, QVector<int>() << Qt::DecorationRole << Qt::DisplayRole );
}


void DesQ::Files::View::copy() {
    IOProcess::Progress *process = new IOProcess::Progress();

    process->sourceDir = DesQ::Utils::dirName( ioArgs.at( 0 ) );
    process->targetDir = ioArgs.takeLast();

    if ( not process->sourceDir.endsWith( "/" ) ) {
        process->sourceDir += "/";
    }

    QStringList srcList;

    foreach( QString file, ioArgs ) {
        if ( DesQ::Utils::exists( file ) ) {
            srcList << file.replace( process->sourceDir, "" );
        }
    }

    if ( not srcList.count() ) {
        return;
    }

    process->type = IOProcess::Type::Copy;

    IODialog *pasteDlg = new IODialog( srcList, process );

    pasteDlg->show();

    ioArgs.clear();
}


void DesQ::Files::View::move() {
    IOProcess::Progress *process = new IOProcess::Progress();

    process->sourceDir = DesQ::Utils::dirName( ioArgs.at( 0 ) );
    process->targetDir = ioArgs.takeLast();

    if ( not process->sourceDir.endsWith( "/" ) ) {
        process->sourceDir += "/";
    }

    QStringList srcList;

    foreach( QString file, ioArgs ) {
        if ( DesQ::Utils::exists( file ) ) {
            srcList << file.replace( process->sourceDir, "" );
        }
    }

    if ( not srcList.count() ) {
        return;
    }

    process->type = IOProcess::Type::Move;

    IODialog *pasteDlg = new IODialog( srcList, process );

    pasteDlg->show();

    ioArgs.clear();
}


void DesQ::Files::View::link() {
    QString path = ioArgs.takeLast();

    foreach( QString node, ioArgs )
    QFile::link( node, QDir( path ).filePath( DesQ::Utils::baseName( node ) ) );

    ioArgs.clear();
}


/**
 * SideView
 */

DesQ::Files::SideView::SideView( QWidget *parent ) : QListView( parent ) {
    setFixedWidth( SideViewWidth );
    setFrameStyle( QFrame::NoFrame );

    /** Icon and grid sizes */
    setIconSize( QSize( 28, 28 ) );

    /** Fixed width, expanding height */
    setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::MinimumExpanding ) );

    /** No scrillbars */
    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    QPalette pltt( palette() );

    pltt.setColor( QPalette::Base, Qt::transparent );
    setPalette( pltt );

    prepareModel();

    connect(
        this, &QListView::clicked, [ = ] ( QModelIndex idx ) {
            QString target( idx.data( Qt::UserRole + 1 ).toString() );

            if ( not target.isEmpty() ) {
                emit load( target );
            }
        }
    );

    connect(
        diskMgr, &DFL::Storage::Manager::volumeAdded, [ = ] ( DFL::Storage::Volume *vol ) {
            qCritical() << "On added:" << vol->label() << vol->mountPoints() << vol->devicePath() << vol->drive();

            QString label( vol->label() );

            if ( label.isEmpty() ) {
                label = vol->devicePath();
            }

            if ( vol->isMountable() == false ) {
                return;
            }

            if ( vol->isMounted() ) {
                QString icon;

                if ( vol->mountPoints().contains( "/" ) ) {
                    icon = "drive-harddisk-root";
                }

                else if ( vol->isRemovable() ) {
                    icon = "drive-removable-media";
                }

                else if ( vol->isRemovable() ) {
                    icon = "media-optical";
                }

                else {
                    icon = "drive-harddisk";
                }

                QStandardItem *item = new QStandardItem( QIcon::fromTheme( icon ), label );
                item->setData( vol->mountPoints().at( 0 ),                                   Qt::UserRole + 1 );
                item->setData( vol->path(),                                                  Qt::UserRole + 2 );
                item->setData( QString( "Mounted at %1" ).arg( vol->mountPoints().at( 0 ) ), Qt::ToolTipRole );
                item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );

                mdl->appendRow( item );
            }

            else {
                qDebug() << "----> Not mounted";
            }
        }
    );

    connect(
        diskMgr, &DFL::Storage::Manager::volumeRemoved, [ = ] ( DFL::Storage::Volume *vol ) {
            QString label = vol->label();

            if ( label.isEmpty() ) {
                label = vol->devicePath();
            }

            for ( QStandardItem *item: mdl->findItems( label ) ) {
                if ( item->data( Qt::UserRole + 2 ).toString() == vol->path() ) {
                    mdl->removeRow( item->row() );
                }
            }
        }
    );
}


void DesQ::Files::SideView::highlight( QString path ) {
    /** Ensure it ends with '/' */
    path += (path.endsWith( "/" ) ? "" : "/");

    /** Clear the selection */
    clearSelection();

    for ( int r = 0; r < mdl->rowCount(); r++ ) {
        QModelIndex idx = mdl->index( r, 0, QModelIndex() );
        QString     tgt = idx.data( Qt::UserRole + 1 ).toString();

        /** Ensure it ends with '/' */
        tgt += (tgt.endsWith( "/" ) ? "" : "/");

        /** Select if we have a match */
        if ( tgt == path ) {
            setCurrentIndex( idx );
            break;
        }
    }
}


void DesQ::Files::SideView::prepareModel() {
    mdl = new QStandardItemModel();

    QStringList bookmarks = { "Home", "Desktop", "Documents", "Downloads", "Music", "Pictures", "Videos" };
    QStringList icons     = {
        "folder-home",      "user-desktop", "folder-documents",
        "folder-downloads", "folder-music", "folder-pictures", "folder-videos"
    };

    QStringList paths = {
        DFL::XDG::homeDir(),
        DFL::XDG::xdgDesktopDir(),
        DFL::XDG::xdgDocumentsDir(),
        DFL::XDG::xdgDownloadDir(),
        DFL::XDG::xdgMusicDir(),
        DFL::XDG::xdgPicturesDir(),
        DFL::XDG::xdgVideosDir(),
    };

    QFont boldFace( font() );

    boldFace.setPointSize( boldFace.pointSize() + 1 );
    boldFace.setWeight( QFont::Bold );

    QStandardItem *item;

    item = new QStandardItem( "Bookmarks" );
    item->setData( boldFace, Qt::FontRole );
    item->setFlags( item->flags() & ~Qt::ItemIsEnabled );
    item->setEnabled( false );
    mdl->appendRow( item );

    for ( int i = 0; i < bookmarks.size(); i++ ) {
        item = new QStandardItem( QIcon::fromTheme( icons.at( i ) ), bookmarks.at( i ) );
        item->setData( paths.at( i ), Qt::UserRole + 1 );
        item->setData( paths.at( i ), Qt::ToolTipRole );
        item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );

        mdl->appendRow( item );
    }

    QStandardItem *blank = new QStandardItem();

    blank->setFlags( blank->flags() & ~Qt::ItemIsEnabled );
    blank->setEnabled( false );
    mdl->appendRow( blank );

    item = new QStandardItem( "Drives" );
    item->setData( boldFace, Qt::FontRole );
    item->setFlags( item->flags() & ~Qt::ItemIsEnabled );
    item->setEnabled( false );
    mdl->appendRow( item );

    for ( DFL::Storage::Volume *vol: diskMgr->allVolumes() ) {
        if ( vol->isMountable() == false ) {
            continue;
        }

        if ( vol->isMounted() ) {
            QString icon;

            if ( vol->mountPoints().contains( "/" ) ) {
                icon = "drive-harddisk-root";
            }

            else if ( vol->isRemovable() ) {
                icon = "drive-removable-media";
            }

            else if ( vol->isRemovable() ) {
                icon = "media-optical";
            }

            else {
                icon = "drive-harddisk";
            }

            item = new QStandardItem( QIcon::fromTheme( icon ), vol->label() );
            item->setData( vol->mountPoints().at( 0 ),                                   Qt::UserRole + 1 );
            item->setData( vol->path(),                                                  Qt::UserRole + 2 );
            item->setData( QString( "Mounted at %1" ).arg( vol->mountPoints().at( 0 ) ), Qt::ToolTipRole );
            item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );

            mdl->appendRow( item );
        }
    }

    setModel( mdl );
}
