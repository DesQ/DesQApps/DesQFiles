/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include <desqui/UI.hpp>

#include "ContextMenu.hpp"

static QIcon getIcon( QString path ) {
    QFileInfo info( path );

    if ( info.isDir() ) {
        return QIcon::fromTheme( "folder" );
    }

    else if ( info.isFile() ) {
        QMimeType mt = mimeDb.mimeTypeForFile( path );
        QIcon     i1 = QIcon::fromTheme( mt.iconName() );

        if ( i1.pixmap( 24 ).width() ) {
            return i1;
        }

        QIcon i2 = QIcon::fromTheme( mt.genericIconName() );

        if ( i2.pixmap( 24 ).width() ) {
            return i2;
        }
    }

    return QIcon::fromTheme( "unknown", QIcon::fromTheme( "application-x-zerosize", QIcon::fromTheme( "application-octet-stream" ) ) );
}


DesQ::Files::ContextMenu::ContextMenu( QWidget *parent ): QWidget( parent ) {
    /**
     * Width: 270px
     * Height: 67px ( 5 x 2 (margins) + 16 (title) + 5 (spacer) + 36 (actions) + 5 (spacer) + 16
     *(context/tooltips etc) )
     */
    setMinimumSize( QSize( 360, 88 ) );

    /** TODO: Remove once everything is ready */
    setFixedSize( QSize( 270, 88 ) );

    /**
     * We will be painting this widget ourselves
     */
    setAttribute( Qt::WA_TranslucentBackground );

    /**
     * Create the fixed UI elements - the text layout:
     */
    createUI();
}


void DesQ::Files::ContextMenu::setSelection( QModelIndexList ) {
}


void DesQ::Files::ContextMenu::reset() {
}


void DesQ::Files::ContextMenu::close() {
}


void DesQ::Files::ContextMenu::createUI() {
}


void DesQ::Files::ContextMenu::updateActions() {
}


void DesQ::Files::ContextMenu::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    QColor pen   = palette().color( QPalette::Window );
    QColor brush = palette().color( QPalette::Window );

    brush.setAlphaF( 0.95 );

    painter.setRenderHints( QPainter::Antialiasing );
    painter.setPen( QPen( pen, 1.0 ) );
    painter.setBrush( brush );

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 5.0, 5.0 );

    painter.end();

    QWidget::paintEvent( pEvent );
}


void DesQ::Files::ContextMenu::keyReleaseEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Escape ) {
        close();

        return;
    }

    QWidget::keyReleaseEvent( kEvent );
}


void DesQ::Files::ContextMenu::mouseMoveEvent( QMouseEvent *mEvent ) {
    QWidget::mouseMoveEvent( mEvent );
}
