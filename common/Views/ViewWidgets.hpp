/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "Model.hpp"

namespace DesQ {
    namespace Files {
        class PathBar;
    }
}

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
#define QMouseEnterEvent    QEvent
#else
#define QMouseEnterEvent    QEnterEvent
#endif

class Separator : public QWidget {
    Q_OBJECT

    public:
        static QWidget * vertical( QWidget *parent   = 0 );
        static QWidget * horizontal( QWidget *parent = 0 );

    private:
        enum Mode {
            Horizontal,
            Vertical
        };

        Separator( Separator::Mode mode, QWidget *parent = 0 );

        QLinearGradient gradient;
        Separator::Mode mMode;

    protected:
        void paintEvent( QPaintEvent * );
        void resizeEvent( QResizeEvent * );
};


class ActionButton : public QAbstractButton {
    Q_OBJECT

    public:
        ActionButton( QWidget *parent );

        /**
         * AutoClose: close the parent on click.
         */
        bool autoClose();
        void setAutoClose( bool yes );

        /** Generate and store the pixmap */
        void setIcon( QIcon );

        /** We will show other actions */
        Q_SIGNAL void showOptions();

        /** Emit a signal when an option is selected */
        Q_SIGNAL void contextualInfo( QString );

        /** Update the pixmap */
        Q_SLOT void setIconSize( QSize );

    private:
        QPixmap mIcon;

        bool mInside = false;

        /** Autoclose is true be default */
        bool mAutoClose = true;

    protected:
        /** Mouse entered this widget */
        void enterEvent( QMouseEnterEvent * ) override;

        /** The mouse left this widget */
        void leaveEvent( QEvent * ) override;

        /** We want a stylish button */
        void paintEvent( QPaintEvent * ) override;
};


class DesQNavButton : public QWidget {
    Q_OBJECT

    public:
        DesQNavButton( QWidget *parent );

        void setBackEnabled( bool );
        void setForwardEnabled( bool );
        void setUpEnabled( bool );

    private:
        QToolButton *prevBtn = nullptr;
        QToolButton *upBtn   = nullptr;
        QToolButton *nextBtn = nullptr;

        QShortcut *prevCut = nullptr;
        QShortcut *upCut   = nullptr;
        QShortcut *nextCut = nullptr;

    protected:
        void paintEvent( QPaintEvent *pEvent ) override;

    Q_SIGNALS:
        void back();
        void up();
        void forward();
};

class DesQ::Files::PathBar : public QLabel {
    Q_OBJECT;

    public:
        PathBar( QWidget *parent );

        void setPath( QString );
};
