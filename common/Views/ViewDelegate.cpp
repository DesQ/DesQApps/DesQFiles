/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ViewDelegate.hpp"
#include "Model.hpp"
#include "View.hpp"


void drawPath( QRectF rect, int line, QPainter *p );


void DesQ::Files::ViewDelegate::paint( QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const {
    if ( index.column() != 0 ) {
        QStyledItemDelegate::paint( painter, option, index );
    }

    else {
        if ( option.decorationPosition == QStyleOptionViewItem::Top ) {
            paintIcons( painter, option, index );
        }

        /* Tile View */
        else if ( option.decorationPosition == QStyleOptionViewItem::Left ) {
            paintTiles( painter, option, index );
        }
    }
}


/**
 * This code is contributed by eyllanesc (https://stackoverflow.com/users/6622587/eyllanesc)
 * In Qt, sometimes there are issues with text eliding in QListView::ListMode. When using
 * uniformItemSizes, the item does not utilize the entire space of the grid available to it.
 * Setting the correct sizeHint based on the gridSize, ensures that complete space is used.
 **/
QSize DesQ::Files::ViewDelegate::sizeHint( const QStyleOptionViewItem& option, const QModelIndex& ) const {
    QSize size( DesQ::Files::View::gridSize );

    /* Icon View */
    if ( option.decorationPosition == QStyleOptionViewItem::Top ) {
        return size - QSize( DesQ::Files::View::extraSpace, 5 );
    }

    /* Tile View */
    else if ( option.decorationPosition == QStyleOptionViewItem::Left ) {
        return size - QSize( 2, 2 );
    }

    return size;
}


void DesQ::Files::ViewDelegate::paintTiles( QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const {
    const DesQ::Files::Model *model = static_cast<const DesQ::Files::Model *>(index.model() );
    QFileInfo                ftype  = model->fileInfo( index );

    QRect optionRect( option.rect );

    // Get icon size
    QSize iconSize( option.decorationSize );

    QString text    = index.data( Qt::DisplayRole ).toString();
    QString altText = index.data( Qt::UserRole + 1 ).toString() + " | " + model->type( index ) + " | " + ftype.owner();

    QPixmap icon;

    if ( option.state & QStyle::State_Selected ) {
        icon = index.data( Qt::DecorationRole ).value<QIcon>().pixmap( iconSize, QIcon::Selected );
    }

    else if ( option.state & QStyle::State_MouseOver ) {
        icon = index.data( Qt::DecorationRole ).value<QIcon>().pixmap( iconSize, QIcon::Active );
    }

    else {
        icon = index.data( Qt::DecorationRole ).value<QIcon>().pixmap( iconSize, QIcon::Normal );
    }

    if ( icon.size() != iconSize ) {
        icon = icon.scaled( iconSize, Qt::KeepAspectRatio, Qt::SmoothTransformation );
    }

    QSize iSize( icon.size() );

    // Icon padding
    qreal padding = iconSize.width() * 0.1;

    QRectF iconRect;

    // Original X + Image Left Border + Width to make the ima
    iconRect.setX( optionRect.x() + padding + (iconSize.width() - iSize.width() ) / 2 );
    // Original Y + Image Top Border + Height to make the image center of the icon rectangle
    iconRect.setY( optionRect.y() + (optionRect.height() - iSize.height() ) / 2 );
    // Icon Size
    iconRect.setSize( iSize );

    /* Selection rectangle */
    painter->save();

    /* Selection painter settings */
    painter->setPen( QPen( Qt::NoPen ) );

    if ( (option.state & QStyle::State_Selected) and (option.state & QStyle::State_MouseOver) ) {
        painter->setBrush( option.palette.color( QPalette::Highlight ).darker( 115 ) );
    }

    else if ( option.state & QStyle::State_Selected ) {
        painter->setBrush( option.palette.color( QPalette::Highlight ) );
    }

    else if ( option.state & QStyle::State_MouseOver ) {
        painter->setBrush( option.palette.color( QPalette::Highlight ).darker( 130 ) );
    }

    else {
        painter->setBrush( QBrush( Qt::transparent ) );
    }

    /* Selection rectangle */
    painter->drawRect( optionRect );

    /* Focus Rectangle - In our case focus under line */
    if ( option.state & QStyle::State_HasFocus ) {
        painter->setBrush( Qt::NoBrush );
        QPoint bl = optionRect.bottomLeft() + QPoint( 7, -1 );
        QPoint br = optionRect.bottomRight() - QPoint( 7, 1 );

        QLinearGradient hLine( bl, br );

        hLine.setColorAt( 0,   Qt::transparent );
        hLine.setColorAt( 0.3, option.palette.color( QPalette::BrightText ) );
        hLine.setColorAt( 0.7, option.palette.color( QPalette::BrightText ) );
        hLine.setColorAt( 1,   Qt::transparent );

        painter->setPen( QPen( QBrush( hLine ), 1 ) );
        painter->drawLine( bl, br );
    }

    painter->restore();

    // Paint Icon
    painter->save();
    painter->setRenderHint( QPainter::Antialiasing, true );
    painter->drawPixmap( iconRect.toRect(), icon );
    painter->restore();

    // Paint emblem
    if ( index.data( Qt::UserRole + 2 ).toInt() == 1 ) {
        QRectF emblemRect( iconRect.bottomRight() - QPoint( 16, 16 ), QSize( 16, 16 ) );
        painter->drawPixmap( emblemRect.toRect(), QIcon::fromTheme( "emblem-symbolic-link" ).pixmap( 16 ) );
    }

    if ( index.data( Qt::UserRole + 2 ).toInt() == 2 ) {
        QRectF emblemRect( iconRect.bottomRight() - QPoint( 16, 16 ), QSize( 16, 16 ) );
        painter->drawPixmap( emblemRect.toRect(), QIcon::fromTheme( "application-x-executable" ).pixmap( 16 ) );
    }

    painter->save();
    // Text Painter Settings
    painter->setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing );

    // Bold text for focussed item
    if ( option.state & QStyle::State_HasFocus ) {
        painter->setFont( QFont( painter->font().family(), painter->font().pointSize(), QFont::Bold ) );
    }

    if ( (option.state & QStyle::State_MouseOver) or (option.state & QStyle::State_Selected) ) {
        painter->setPen( option.palette.color( QPalette::HighlightedText ) );
    }

    else {
        painter->setPen( QPen( index.data( Qt::ForegroundRole ).value<QBrush>(), 1 ) );
    }

    QRectF textRect;

    // Original X + Image Width + Image-padding
    textRect.setX( optionRect.x() + iconSize.width() + 2 * padding );
    // Original Width - Image Left Border - Image Width - Image Text Gap - Text Right Border
    textRect.setWidth( optionRect.width() - iconSize.width() - 3 * padding );

    /* Font Mentrics for elided text */
    QFontMetrics fm( painter->font() );

    text = fm.elidedText( text, Qt::ElideRight, textRect.width() );

    /** Draw only the main text */
    if ( iconSize.width() < 32 ) {
        // Vertical Centering, so don't bother
        textRect.setY( optionRect.y() );

        // Original Height
        textRect.setHeight( optionRect.height() );

        painter->drawText( textRect, Qt::AlignVCenter, text );
    }

    /** Draw extra text as well */
    else {
        qreal textHeight = fm.boundingRect( text ).height() * 2;            // We have two lines of text
        qreal vPad       = (optionRect.height() - textHeight) / 2;          // Top and bottom padding

        // Line 1 of text: Original Y + Padding
        textRect.setY( optionRect.y() + vPad );
        // Original Width - Image Left Border - Image Width - Image Text Gap - Text Right Border
        textRect.setWidth( optionRect.width() - iconSize.width() - 3 * padding );
        // Line height
        textRect.setHeight( textHeight / 2.0 );

        // Draw line 1 of the text
        painter->drawText( textRect, Qt::AlignVCenter, text );

        // Line 2: 2 pt less than regular size
        painter->setFont( QFont( painter->font().family(), painter->font().pointSize() - 2 ) );
        QFontMetrics fm2( painter->font() );
        altText = fm2.elidedText( altText, Qt::ElideRight, textRect.width() );

        // Line 2 of text: Original Y + Padding + Line Height + Line Spacing
        textRect.setY( optionRect.y() + vPad + textHeight / 2.0 );
        // Line height
        textRect.setHeight( textHeight / 2.0 );

        painter->drawText( textRect, Qt::AlignVCenter, altText );
    }

    painter->restore();
}


void DesQ::Files::ViewDelegate::paintIcons( QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const {
    const DesQ::Files::Model *model = static_cast<const DesQ::Files::Model *>(index.model() );
    QFileInfo                ftype  = model->fileInfo( index );

    QRect optionRect( option.rect );

    /* Get icon size */
    QSize iconSize( option.decorationSize );

    /* Text to be displayed */
    QString text = index.data( Qt::DisplayRole ).toString();

    /* Icon to be painted */
    QPixmap icon;

    if ( option.state & QStyle::State_Selected ) {
        icon = index.data( Qt::DecorationRole ).value<QIcon>().pixmap( iconSize, QIcon::Selected );
    }

    else if ( option.state & QStyle::State_MouseOver ) {
        icon = index.data( Qt::DecorationRole ).value<QIcon>().pixmap( iconSize, QIcon::Active );
    }

    else {
        icon = index.data( Qt::DecorationRole ).value<QIcon>().pixmap( iconSize, QIcon::Normal );
    }

    if ( (icon.size().width() > iconSize.width() ) or (icon.size().height() > iconSize.height() ) ) {
        icon = icon.scaled( iconSize, Qt::KeepAspectRatio, Qt::SmoothTransformation );
    }

    /* Icon Size */
    QSize iSize( icon.size() );

    // Icon padding
    qreal padding = iconSize.width() * 0.1;

    QRectF textRect;

    // Horizontal Centering, so don't bother
    textRect.setX( optionRect.x() + padding );
    // Original Y + Image Height + Image Padding Top + Text-Image Padding ( fixed = 5 )
    textRect.setY( optionRect.y() + padding + 5 + iconSize.height() );
    // Left and Right Border
    textRect.setSize( optionRect.size() - QSize( 2 * padding, qMin( 3.0, padding / 2.0 ) + padding + iconSize.height() ) );

    /* Icon rect */
    QRectF iconRect;

    // Original X
    iconRect.setX( optionRect.x() + (optionRect.width() - iSize.width() ) / 2.0 );
    // Original Y + Image Top Border + Height to make the image center of the icon rectangle
    iconRect.setY( optionRect.y() + padding + (iconSize.height() - iSize.height() ) / 2.0 );
    // Icon Size
    iconRect.setSize( iSize );

    /* Selection rectangle */
    painter->save();

    painter->setRenderHints( QPainter::Antialiasing );

    /* Selection painter settings */
    painter->setPen( QPen( Qt::NoPen ) );

    /* Selection rectangle */
    drawPath( QRectF( optionRect ), textRect.top(), painter );

    /* Focus Rectangle - In our case focus under line */
    if ( option.state & QStyle::State_HasFocus ) {
        painter->setBrush( Qt::NoBrush );
        QPoint bl = optionRect.bottomLeft() + QPoint( 7, -1 );
        QPoint br = optionRect.bottomRight() - QPoint( 7, 1 );

        QLinearGradient hLine( bl, br );

        hLine.setColorAt( 0,   Qt::transparent );
        hLine.setColorAt( 0.3, option.palette.color( QPalette::BrightText ) );
        hLine.setColorAt( 0.7, option.palette.color( QPalette::BrightText ) );
        hLine.setColorAt( 1,   Qt::transparent );

        painter->setPen( QPen( QBrush( hLine ), 1 ) );
        painter->drawLine( bl, br );
    }

    painter->restore();

    // Paint Icon
    painter->save();
    painter->setRenderHint( QPainter::Antialiasing, true );
    painter->drawPixmap( iconRect.toRect(), icon );
    painter->restore();

    // Paint emblem
    if ( index.data( Qt::UserRole + 2 ).toInt() == 1 ) {
        QRectF emblemRect( iconRect.bottomRight() - QPoint( 16, 16 ), QSize( 16, 16 ) );
        painter->drawPixmap( emblemRect.toRect(), QIcon::fromTheme( "emblem-symbolic-link" ).pixmap( 16 ) );
    }

    if ( index.data( Qt::UserRole + 2 ).toInt() == 2 ) {
        QRectF emblemRect( iconRect.bottomRight() - QPoint( 16, 16 ), QSize( 16, 16 ) );
        painter->drawPixmap( emblemRect.toRect(), QIcon::fromTheme( "application-x-executable" ).pixmap( 16 ) );
    }

    painter->save();
    // Text Painter Settings
    painter->setPen( option.palette.color( QPalette::Text ) );

    // Draw Text
    if ( option.state & QStyle::State_HasFocus ) {
        painter->setFont( QFont( painter->font().family(), painter->font().pointSize(), QFont::Bold ) );
    }

    if ( (option.state & QStyle::State_MouseOver) or (option.state & QStyle::State_Selected) ) {
        painter->setPen( option.palette.color( QPalette::HighlightedText ) );
    }

    else {
        painter->setPen( QPen( index.data( Qt::ForegroundRole ).value<QBrush>(), 1 ) );
    }

    /* Font Mentrics for elided text */
    QFontMetrics fm( painter->font() );

    text = fm.elidedText( text, Qt::ElideRight, textRect.width() );
    painter->drawText( textRect.adjusted( 0, 2, 0, 2 ), Qt::AlignHCenter, text );

    /* One detail */
    int lineSpacing = (textRect.height() - QFontInfo( qApp->font() ).pixelSize() * 2) / 3;

    textRect.adjust( 0, lineSpacing + QFontInfo( qApp->font() ).pixelSize(), 0, 0 );
    QString detail = index.data( Qt::UserRole + 1 ).toString() + " | " + model->type( index );

    painter->setFont( QFont( painter->font().family(), painter->font().pointSize() - 2 ) );
    QFontMetrics fm2( painter->font() );

    detail = fm2.elidedText( detail, Qt::ElideRight, textRect.width() );

    painter->drawText( textRect.adjusted( 0, 2, 0, 2 ), Qt::AlignHCenter, detail );

    painter->restore();

    if ( option.state & QStyle::State_Selected ) {
        painter->save();

        painter->setRenderHints( QPainter::Antialiasing );
        painter->setPen( Qt::NoPen );
        painter->setBrush( option.palette.color( QPalette::Highlight ) );
        int ptSz = option.font.pointSize();
        painter->drawEllipse( QRectF( optionRect.x() + 5, optionRect.y() + 5, ptSz + 5, ptSz + 5 ) );

        painter->setPen( option.palette.color( QPalette::HighlightedText ) );
        painter->setBrush( Qt::NoBrush );
        painter->drawText( QRectF( optionRect.x() + 5, optionRect.y() + 5, ptSz + 5, ptSz + 5 ), Qt::AlignCenter, "✓" );

        painter->setPen( option.palette.color( QPalette::Highlight ) );
        painter->drawRoundedRect( QRectF( optionRect ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );

        painter->restore();
    }

    else if ( option.state & QStyle::State_MouseOver ) {
        painter->save();

        painter->setRenderHints( QPainter::Antialiasing );
        painter->setOpacity( 0.25 );

        painter->setPen( Qt::NoPen );
        painter->setBrush( option.palette.color( QPalette::Highlight ) );
        int ptSz = option.font.pointSize();
        painter->drawEllipse( QRectF( optionRect.x() + 5, optionRect.y() + 5, ptSz + 5, ptSz + 5 ) );

        painter->setPen( option.palette.color( QPalette::HighlightedText ) );
        painter->setBrush( Qt::NoBrush );
        painter->drawText( QRectF( optionRect.x() + 5, optionRect.y() + 5, ptSz + 5, ptSz + 5 ), Qt::AlignCenter, "✓" );

        painter->setPen( option.palette.color( QPalette::Highlight ) );
        painter->drawRoundedRect( QRectF( optionRect ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );

        painter->restore();
    }
}


void drawPath( QRectF rect, int line, QPainter *p ) {
    QPainterPath path;
    QRectF       iconRect( rect.x(), rect.y(), rect.width(), line - rect.y() );

    path.setFillRule( Qt::WindingFill );
    path.addRoundedRect( iconRect, 3, 3 );
    path.addRect( QRectF( rect.x(), rect.y() + 3, rect.width(), line - 3 - rect.y() ) );

    p->save();
    p->setRenderHints( QPainter::Antialiasing );
    p->setPen( Qt::NoPen );
    p->setBrush( QColor( 90, 90, 90, 60 ) );
    p->drawRoundedRect( rect, 3, 3 );
    p->setBrush( QColor( 0, 0, 0, 60 ) );
    p->drawPath( path.simplified() );
    p->restore();
}
