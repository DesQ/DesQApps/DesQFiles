/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>

#include "ViewDialogs.hpp"

DesQ::Files::Dialog::Dialog( QWidget *parent ): QWidget( parent ) {
    mParent   = parent;
    mAccepted = false;

    createUI();

    resize( parent->size() );
}


void DesQ::Files::Dialog::createUI() {
    mView = new QListView( this );
    mView->setFrameStyle( QFrame::NoFrame );
    mView->setSelectionBehavior( QAbstractItemView::SelectItems );
    mView->setWordWrap( false );
    mView->setTextElideMode( Qt::ElideRight );

    mEdit = new QLineEdit( this );

    mTitle = new QLabel( this );
    mInfo  = new QLabel( this );
    mInfo->setWordWrap( true );

    mEditInfo = new QLabel( this );
    mEditInfo->setBuddy( mEdit );

    okBtn = new QToolButton( this );
    okBtn->setIcon( QIcon::fromTheme( "dialog-ok" ) );
    connect(
        okBtn, &QToolButton::clicked, [ = ] () mutable {
            mAccepted = true;
            close();
            emit closed();
        }
    );

    cancelBtn = new QToolButton( this );
    cancelBtn->setIcon( QIcon::fromTheme( "dialog-cancel" ) );
    connect(
        cancelBtn, &QToolButton::clicked, [ = ] () mutable {
            mAccepted = false;
            close();
            emit closed();
        }
    );

    QHBoxLayout *btnLyt = new QHBoxLayout();

    btnLyt->addStretch();
    btnLyt->addWidget( okBtn );
    btnLyt->addWidget( cancelBtn );

    mExtra = nullptr;

    mMainLyt = new QGridLayout();

    mMainLyt->addWidget( mTitle, 0, 0, 1, 2 );
    mMainLyt->addWidget( mInfo,  1, 0, 1, 2 );

    mMainLyt->addWidget( mView,  2, 0, 1, 2 );

    // -> Vacancy for mExtra, if it's set

    mMainLyt->addWidget( mEditInfo, 4, 0 );
    mMainLyt->addWidget( mEdit,     4, 1 );

    mMainLyt->addLayout( btnLyt, 5, 1 );

    mDialog = new QWidget();
    mDialog->setObjectName( "dialog" );
    mDialog->setLayout( mMainLyt );
    mDialog->setFixedSize( 360, 450 );

    QWidget *base = new QWidget();

    base->setObjectName( "dialog-base" );

    QHBoxLayout *baseLyt = new QHBoxLayout();

    baseLyt->setAlignment( Qt::AlignCenter );
    baseLyt->addWidget( mDialog );

    setLayout( baseLyt );
}


void DesQ::Files::Dialog::showListView( bool yes ) {
    if ( yes ) {
        mView->show();
    }

    else {
        mView->hide();
    }
}


void DesQ::Files::Dialog::showLineEdit( bool yes ) {
    if ( yes ) {
        mEdit->show();
    }

    else {
        mEdit->hide();
    }
}


void DesQ::Files::Dialog::setExtraWidget( QWidget *w ) {
    if ( mExtra ) {
        mMainLyt->removeWidget( mExtra );
        delete mExtra;
    }

    mExtra = w;
    mMainLyt->addWidget( mExtra, 3, 0, 1, 2 );
}


void DesQ::Files::Dialog::setModel( QStandardItemModel *mdl ) {
    mView->setModel( mdl );
}


void DesQ::Files::Dialog::setTitle( QString title ) {
    mTitle->setText( title );
}


void DesQ::Files::Dialog::setDescription( QString descr ) {
    mInfo->setText( descr );
}


void DesQ::Files::Dialog::setEditInfo( QString info ) {
    mEditInfo->setText( info );
}


void DesQ::Files::Dialog::setPlaceholderText( QString txt ) {
    mEdit->setPlaceholderText( txt );
}


void DesQ::Files::Dialog::show() {
    resize( mParent->size() );

    QWidget::show();
}


int DesQ::Files::Dialog::exec() {
    QEventLoop loop;

    connect( this, &DesQ::Files::Dialog::closed, &loop, &QEventLoop::quit );

    if ( not isVisible() ) {
        show();
    }

    loop.exec();

    return (mAccepted ? 1 : 0);
}


void DesQ::Files::Dialog::keyPressEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Escape ) {
        close();
    }

    QWidget::keyPressEvent( kEvent );
}


void DesQ::Files::Dialog::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    QColor bg1( 0, 0, 0, 150 );

    painter.setPen( Qt::NoPen );
    painter.setBrush( bg1 );
    painter.drawRect( rect() );

    painter.setRenderHints( QPainter::Antialiasing );
    QColor bg2( palette().color( QPalette::Window ) );

    painter.setPen( QPen( Qt::gray, 1.0 ) );
    painter.setBrush( bg2 );
    painter.drawRoundedRect( QRectF( mDialog->geometry() ).adjusted( -0.5, -0.5, 0.5, 0.5 ), 3, 3 );

    painter.end();

    QWidget::paintEvent( pEvent );
}


void DesQ::Files::Dialog::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        if ( not mDialog->underMouse() ) {
            mAccepted = false;
            close();
            emit closed();
        }
    }

    QWidget::mouseReleaseEvent( mEvent );
}


QModelIndexList DesQ::Files::Dialog::selectedIndexes() {
    QModelIndexList sels = mView->selectionModel()->selectedIndexes();

    if ( not sels.count() ) {
        sels << mView->currentIndex();
    }

    return sels;
}
