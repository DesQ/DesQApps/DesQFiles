/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include <desqui/UI.hpp>

#include "ContextMenu.hpp"


static QIcon getIcon( QString path ) {
    QFileInfo info( path );

    if ( info.isDir() ) {
        return QIcon::fromTheme( "folder" );
    }

    else if ( info.isFile() ) {
        QMimeType mt = mimeDb.mimeTypeForFile( path );
        QIcon     i1 = QIcon::fromTheme( mt.iconName() );

        if ( i1.pixmap( 24 ).width() ) {
            return i1;
        }

        QIcon i2 = QIcon::fromTheme( mt.genericIconName() );

        if ( i2.pixmap( 24 ).width() ) {
            return i2;
        }
    }

    return QIcon::fromTheme( "unknown", QIcon::fromTheme( "application-x-zerosize", QIcon::fromTheme( "application-octet-stream" ) ) );
}


void populateOpenBase( QWidget *base, QWidget *parent, QString path ) {
    /** Clear the existing layout first */
    if ( base->layout() ) {
        QWidget *tmp = new QWidget();
        tmp->setLayout( base->layout() );

        delete tmp;
    }

    ActionButton *openWithBtn = new ActionButton( parent );
    openWithBtn->setToolTip( "Click to select the application to open the selected item(s)" );
    openWithBtn->setIcon( QIcon::fromTheme( "document-open" ) );
    openWithBtn->setIconSize( QSize( 56, 56 ) );
    openWithBtn->setFixedSize( QSize( 72, 72 ) );

    QHBoxLayout *openLyt = new QHBoxLayout();
    openLyt->setContentsMargins( QMargins() );
    openLyt->setSpacing( 5 );

    openLyt->addWidget( Separator::vertical() );
    openLyt->addWidget( openWithBtn );
    openLyt->addStretch();

    base->setLayout( openLyt );
}


void populateNewBase( QWidget *base, QWidget *parent, QString path ) {
    /** Clear the existing layout first */
    if ( base->layout() ) {
        QWidget *tmp = new QWidget();
        tmp->setLayout( base->layout() );

        delete tmp;
    }

    ActionButton *newFolderBtn = new ActionButton( parent );
    openWithBtn->setToolTip( "Click to create a new fodler here" );
    openWithBtn->setIcon( QIcon::fromTheme( "document-open" ) );
    openWithBtn->setIconSize( QSize( 56, 56 ) );
    openWithBtn->setFixedSize( QSize( 72, 72 ) );

    QHBoxLayout *openLyt = new QHBoxLayout();
    openLyt->setContentsMargins( QMargins() );
    openLyt->setSpacing( 5 );

    openLyt->addWidget( Separator::vertical() );
    openLyt->addWidget( openWithBtn );
    openLyt->addStretch();

    base->setLayout( openLyt );
}


DesQ::Files::ContextMenu::ContextMenu( QWidget *parent ): QWidget( parent ) {
    /** We will use this as a popup */
    setWindowFlags( Qt::Popup );

    /** Translucent background */
    setAttribute( Qt::WA_TranslucentBackground );

    /** We'll create the UI and keep it ready */
    createUI();

    /** So that we'll repaint when mouse enters */
    setMouseTracking( true );
}


void DesQ::Files::ContextMenu::setSelection( QString path ) {
    mPath = path;

    folderCount = -1;
    filesCount  = -1;
    otherCount  = -1;

    updateActions();
}


void DesQ::Files::ContextMenu::setSelection( int folder, int files, int other ) {
    folderCount = folder;
    filesCount  = files;
    otherCount  = other;

    /** Clear the path, if set. */
    mPath = QString();

    updateActions();
}


void DesQ::Files::ContextMenu::clearSelection() {
    titleIcon->clear();
    titleText->clear();

    mPath       = QString();
    folderCount = -1;
    filesCount  = -1;
    otherCount  = -1;
}


void DesQ::Files::ContextMenu::close() {
    openBase->hide();
    cutBase->hide();
    copyBase->hide();
    linkBase->hide();
    termBase->hide();
    textBase->show();
    trashBase->hide();
    newBase->hide();

    setFixedHeight( 104 );
    setFocus();

    QWidget::close();
}


void DesQ::Files::ContextMenu::createUI() {
    titleIcon = new QLabel( this );
    titleText = new QLabel( this );

    openBtn = new ActionButton( this );
    openBtn->setIcon( QIcon::fromTheme( "quickopen" ) );
    openBtn->setIconSize( QSize( 28, 28 ) );
    openBtn->setFixedSize( QSize( 36, 36 ) );
    openBtn->setToolTip( "Click to open" );

    cutBtn = new ActionButton( this );
    cutBtn->setIcon( QIcon::fromTheme( "edit-cut" ) );
    cutBtn->setIconSize( QSize( 28, 28 ) );
    cutBtn->setFixedSize( QSize( 36, 36 ) );
    cutBtn->setToolTip( "Click to cut the selection" );

    copyBtn = new ActionButton( this );
    copyBtn->setIcon( QIcon::fromTheme( "edit-copy" ) );
    copyBtn->setIconSize( QSize( 28, 28 ) );
    copyBtn->setFixedSize( QSize( 36, 36 ) );
    copyBtn->setToolTip( "Click to copt the selection" );

    pasteBtn = new ActionButton( this );
    pasteBtn->setIcon( QIcon::fromTheme( "edit-paste" ) );
    pasteBtn->setIconSize( QSize( 28, 28 ) );
    pasteBtn->setFixedSize( QSize( 36, 36 ) );
    pasteBtn->setToolTip( "Click to paste the clipboard contents" );

    linkBtn = new ActionButton( this );
    linkBtn->setIcon( QIcon::fromTheme( "edit-link" ) );
    linkBtn->setIconSize( QSize( 28, 28 ) );
    linkBtn->setFixedSize( QSize( 36, 36 ) );
    linkBtn->setToolTip( "Click to create a soft-link" );

    renameBtn = new ActionButton( this );
    renameBtn->setIcon( QIcon::fromTheme( "edit-rename" ) );
    renameBtn->setIconSize( QSize( 28, 28 ) );
    renameBtn->setFixedSize( QSize( 36, 36 ) );
    renameBtn->setToolTip( "Click to rename" );
    renameBtn->setAutoClose( false );

    termBtn = new ActionButton( this );
    termBtn->setIcon( QIcon::fromTheme( "utilities-terminal" ) );
    termBtn->setIconSize( QSize( 28, 28 ) );
    termBtn->setFixedSize( QSize( 36, 36 ) );
    termBtn->setToolTip( "Click to open a terminal here" );

    trashBtn = new ActionButton( this );
    trashBtn->setIcon( QIcon::fromTheme( "trash-empty" ) );
    trashBtn->setIconSize( QSize( 28, 28 ) );
    trashBtn->setFixedSize( QSize( 36, 36 ) );
    trashBtn->setToolTip( "Click to send to trash" );

    newItemBtn = new ActionButton( this );
    newItemBtn->setIcon( QIcon::fromTheme( "list-add" ) );
    newItemBtn->setIconSize( QSize( 28, 28 ) );
    newItemBtn->setFixedSize( QSize( 36, 36 ) );
    newItemBtn->setToolTip( "Click to create a new node" );
    newItemBtn->setAutoClose( false );

    propsBtn = new ActionButton( this );
    propsBtn->setIcon( QIcon::fromTheme( "document-properties" ) );
    propsBtn->setIconSize( QSize( 28, 28 ) );
    propsBtn->setFixedSize( QSize( 36, 36 ) );
    propsBtn->setToolTip( "Click to see the properties of the selection" );

    okBtn = new ActionButton( this );
    okBtn->setIcon( QIcon::fromTheme( "dialog-ok" ) );
    okBtn->setIconSize( QSize( 24, 24 ) );
    okBtn->setFixedSize( QSize( 34, 34 ) );

    cancelBtn = new ActionButton( this );
    cancelBtn->setIcon( QIcon::fromTheme( "dialog-close" ) );
    cancelBtn->setIconSize( QSize( 24, 24 ) );
    cancelBtn->setFixedSize( QSize( 34, 34 ) );
    cancelBtn->setAutoClose( false );

    textLE = new QLineEdit( this );
    textLE->setFixedHeight( 33 );

    contextLbl = new QLabel( this );

    /** Secondary bases */
    openBase = new QWidget( this );
    openBase->setFixedHeight( 72 );
    openBase->hide();

    cutBase = new QWidget( this );
    cutBase->setFixedHeight( 72 );
    cutBase->hide();

    ActionButton *moveToBtn = new ActionButton( this );
    moveToBtn->setIcon( QIcon::fromTheme( "edit-move" ) );
    moveToBtn->setFixedSize( QSize( 72, 72 ) );
    moveToBtn->setIconSize( QSize( 56, 56 ) );
    moveToBtn->setToolTip( "Click to move the selection to a target folder" );

    QHBoxLayout *cutLyt = new QHBoxLayout();
    cutLyt->setContentsMargins( QMargins() );
    cutLyt->setSpacing( 5 );

    cutLyt->addWidget( moveToBtn );
    cutLyt->addStretch();
    cutBase->setLayout( cutLyt );

    copyBase = new QWidget( this );
    copyBase->setFixedHeight( 72 );
    copyBase->hide();

    ActionButton *copyUrlBtn = new ActionButton( this );
    copyUrlBtn->setIcon( QIcon::fromTheme( "edit-copy-path" ) );
    copyUrlBtn->setFixedSize( QSize( 72, 72 ) );
    copyUrlBtn->setIconSize( QSize( 56, 56 ) );
    copyUrlBtn->setToolTip( "Click to copy the file path as text" );

    ActionButton *copyToBtn = new ActionButton( this );
    copyToBtn->setIcon( QIcon::fromTheme( "edit-copy" ) );
    copyToBtn->setFixedSize( QSize( 72, 72 ) );
    copyToBtn->setIconSize( QSize( 56, 56 ) );
    copyToBtn->setToolTip( "Click to copy the selection to a target folder" );

    QHBoxLayout *copyLyt = new QHBoxLayout();
    copyLyt->setContentsMargins( QMargins() );
    copyLyt->setSpacing( 5 );

    copyLyt->addWidget( copyUrlBtn );
    copyLyt->addWidget( copyToBtn );
    copyLyt->addStretch();
    copyBase->setLayout( copyLyt );

    linkBase = new QWidget( this );
    linkBase->setFixedHeight( 72 );
    linkBase->hide();

    ActionButton *hardLinkBtn = new ActionButton( this );
    hardLinkBtn->setIcon( QIcon::fromTheme( "edit-link" ) );
    hardLinkBtn->setFixedSize( QSize( 72, 72 ) );
    hardLinkBtn->setIconSize( QSize( 56, 56 ) );
    hardLinkBtn->setToolTip( "Click to create a hard link" );

    QHBoxLayout *linkLyt = new QHBoxLayout();
    linkLyt->setContentsMargins( QMargins() );
    linkLyt->setSpacing( 5 );

    linkLyt->addWidget( hardLinkBtn );
    linkLyt->addStretch();
    linkBase->setLayout( linkLyt );

    termBase = new QWidget( this );
    termBase->setFixedHeight( 72 );
    termBase->hide();

    ActionButton *rootTermBtn = new ActionButton( this );
    rootTermBtn->setIcon( QIcon::fromTheme( "utilities-terminal" ) );
    rootTermBtn->setFixedSize( QSize( 72, 72 ) );
    rootTermBtn->setIconSize( QSize( 56, 56 ) );
    rootTermBtn->setToolTip( "Click to open root terminal here" );

    QHBoxLayout *termLyt = new QHBoxLayout();
    termLyt->setContentsMargins( QMargins() );
    termLyt->setSpacing( 5 );

    termLyt->addWidget( rootTermBtn );
    termLyt->addStretch();
    termBase->setLayout( termLyt );

    textBase = new QWidget( this );
    textBase->setFixedHeight( 72 );
    textBase->hide();

    QGridLayout *textLyt = new QGridLayout();
    textLyt->setContentsMargins( QMargins() );
    textLyt->setSpacing( 5 );

    textLyt->addWidget( textLE,    0, 0, 1, 3 );
    textLyt->addWidget( okBtn,     1, 2 );
    textLyt->addWidget( cancelBtn, 1, 1 );

    textBase->setLayout( textLyt );

    trashBase = new QWidget( this );
    trashBase->setFixedHeight( 72 );
    trashBase->hide();

    ActionButton *trashDeleteBtn = new ActionButton( this );
    trashDeleteBtn->setIcon( QIcon::fromTheme( "edit-delete" ) );
    trashDeleteBtn->setFixedSize( QSize( 72, 72 ) );
    trashDeleteBtn->setIconSize( QSize( 56, 56 ) );
    trashDeleteBtn->setToolTip( "Click to permanently delete the selection" );

    QHBoxLayout *trashLyt = new QHBoxLayout();
    trashLyt->setContentsMargins( QMargins() );
    trashLyt->setSpacing( 5 );

    trashLyt->addWidget( trashDeleteBtn );
    trashLyt->addStretch();
    trashBase->setLayout( trashLyt );

    newBase = new QWidget( this );
    newBase->setFixedHeight( 72 );
    newBase->hide();

    // newBase->setLayout( prepareNewLayout() );

    /** Title Layout */
    QHBoxLayout *titleLyt = new QHBoxLayout();
    titleLyt->setContentsMargins( QMargins() );
    titleLyt->setSpacing( 5 );

    titleLyt->addWidget( titleIcon );
    titleLyt->addWidget( titleText );
    titleLyt->addStretch();

    /** First set of actions */
    QHBoxLayout *firstActLyt = new QHBoxLayout();
    firstActLyt->setContentsMargins( QMargins() );
    firstActLyt->setSpacing( 5 );

    firstActLyt->addWidget( openBtn );

    firstActLyt->addWidget( Separator::vertical() );
    firstActLyt->addWidget( cutBtn );
    firstActLyt->addWidget( copyBtn );
    firstActLyt->addWidget( pasteBtn );
    firstActLyt->addWidget( linkBtn );

    firstActLyt->addWidget( Separator::vertical() );
    firstActLyt->addWidget( renameBtn );

    firstActLyt->addWidget( Separator::vertical() );
    firstActLyt->addWidget( termBtn );

    firstActLyt->addWidget( Separator::vertical() );
    firstActLyt->addWidget( trashBtn );

    firstActLyt->addWidget( Separator::vertical() );
    firstActLyt->addWidget( newItemBtn );

    firstActLyt->addStretch();
    firstActLyt->addWidget( propsBtn );

    /** Base Layout */
    QVBoxLayout *baseLyt = new QVBoxLayout();
    baseLyt->setSpacing( 5 );

    baseLyt->addLayout( titleLyt );
    baseLyt->addLayout( firstActLyt );

    baseLyt->addWidget( Separator::horizontal() );

    baseLyt->addWidget( contextLbl );
    baseLyt->addWidget( openBase );
    baseLyt->addWidget( cutBase );
    baseLyt->addWidget( copyBase );
    baseLyt->addWidget( linkBase );
    baseLyt->addWidget( termBase );
    baseLyt->addWidget( textBase );
    baseLyt->addWidget( trashBase );
    baseLyt->addWidget( newBase );

    setLayout( baseLyt );

    connect( openBtn,    &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( cutBtn,     &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( copyBtn,    &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( pasteBtn,   &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( linkBtn,    &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( renameBtn,  &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( termBtn,    &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( trashBtn,   &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( newItemBtn, &ActionButton::contextualInfo, contextLbl, &QLabel::setText );
    connect( propsBtn,   &ActionButton::contextualInfo, contextLbl, &QLabel::setText );

    connect(
        openBtn, &ActionButton::showOptions, [ = ] () {
            openBase->show();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->hide();
            trashBase->hide();
            newBase->hide();

            openBase->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        cutBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->show();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->hide();
            trashBase->hide();
            newBase->hide();

            cutBase->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        copyBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->show();
            linkBase->hide();
            termBase->hide();
            textBase->hide();
            trashBase->hide();
            newBase->hide();

            copyBase->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        pasteBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->show();
            trashBase->hide();
            newBase->hide();

            textLE->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        linkBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->show();
            termBase->hide();
            textBase->hide();
            trashBase->hide();
            newBase->hide();

            linkBase->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        renameBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->show();
            trashBase->hide();
            newBase->hide();

            setFixedHeight( 181 );
        }
    );

    connect(
        renameBtn, &ActionButton::clicked, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->show();
            trashBase->hide();
            newBase->hide();

            textLE->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        termBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->show();
            textBase->hide();
            trashBase->hide();
            newBase->hide();

            termBase->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        trashBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->hide();
            trashBase->show();
            newBase->hide();

            trashBase->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        newItemBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->hide();
            trashBase->hide();
            newBase->show();

            newBase->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        newItemBtn, &ActionButton::clicked, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->hide();
            trashBase->hide();
            newBase->show();

            newBase->setFocus();
            setFixedHeight( 181 );
        }
    );

    connect(
        propsBtn, &ActionButton::showOptions, [ = ] () {
            openBase->hide();
            cutBase->hide();
            copyBase->hide();
            linkBase->hide();
            termBase->hide();
            textBase->hide();
            trashBase->hide();
            newBase->hide();

            setFixedHeight( 104 );
        }
    );
}


void DesQ::Files::ContextMenu::updateActions() {
    int files = 0, folders = 0, others = 0;

    // Single selection
    if ( mPath.length() ) {
        QFileInfo fInfo( mPath );
        QIcon     icon = getIcon( mPath );
        titleIcon->setPixmap( icon.pixmap( 48 ).scaled( QSize( 16, 16 ), Qt::KeepAspectRatio, Qt::SmoothTransformation ) );
        titleText->setText( fInfo.fileName() );

        if ( fInfo.isDir() ) {
            folders++;
        }

        else if ( fInfo.isFile() ) {
            files++;
        }

        else {
            others++;
        }
    }

    else {
        titleIcon->setPixmap( QIcon::fromTheme( "document-multiple" ).pixmap( 48 ).scaled( QSize( 16, 16 ), Qt::KeepAspectRatio, Qt::SmoothTransformation ) );
        QString text;

        if ( folderCount ) {
            text    = QString( "%1 folder%2" ).arg( folderCount ).arg( folderCount == 1 ? "" : "s" );
            folders = folderCount;
        }

        if ( filesCount ) {
            text += QString( "%1 %2 file%2" ).arg( text.length() ? "" : "," ).arg( filesCount ).arg( filesCount == 1 ? "" : "s" );
            files = filesCount;
        }

        if ( otherCount ) {
            text  += QString( "%1 %2 other%2" ).arg( text.length() ? "" : " and" ).arg( otherCount ).arg( otherCount == 1 ? "" : "s" );
            others = otherCount;
        }

        titleText->setText( text );
    }

    populateOpenBase( openBase, this, QString() );

    cutBtn->show();
    copyBtn->show();

    /**
     * When we have a single folder selection
     */
    if ( (folders == 1) && (files == 0) && (others == 0) ) {
        pasteBtn->show();
        termBtn->show();
        newItemBtn->show();

        // openBtn->addOption( "new-window", QIcon::fromTheme( "window-new" ) );
        // openBtn->addOption( "new-tab", QIcon::fromTheme( "tab-new" ) );
    }

    else if ( (folders == 0) && (files == 1) && (others == 0) ) {
        // openBtn->addOption( "pulsar", QIcon::fromTheme( "pulsar" ) );
        // openBtn->addOption( "geany", QIcon::fromTheme( "geany" ) );
    }

    else {
        pasteBtn->hide();
        termBtn->hide();
        newItemBtn->hide();
    }
}


void DesQ::Files::ContextMenu::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    QColor pen   = palette().color( QPalette::Window );
    QColor brush = palette().color( QPalette::Window );

    brush.setAlphaF( 0.95 );

    painter.setRenderHints( QPainter::Antialiasing );
    painter.setPen( QPen( pen, 1.0 ) );
    painter.setBrush( brush );

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 5.0, 5.0 );

    painter.end();

    QWidget::paintEvent( pEvent );
}


void DesQ::Files::ContextMenu::mouseMoveEvent( QMouseEvent *mEvent ) {
    for ( ActionButton *btn: findChildren<ActionButton *>() ) {
        if ( btn->underMouse() ) {
            btn->update();
            emit btn->showOptions();
            emit btn->contextualInfo( btn->toolTip() );
        }

        else {
            btn->update();
        }
    }

    QWidget::mouseMoveEvent( mEvent );
}
