/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "Model.hpp"
#include "ViewWidgets.hpp"

namespace DesQ {
    namespace Files {
        class ContextMenu;
    }
}


// class TextLayout: public QWidget {
//     Q_OBJECT
//
//     TextLayout( QWidget *parent );
//
//     QString fileName();
// };


class DesQ::Files::ContextMenu : public QWidget {
    Q_OBJECT

    public:
        ContextMenu( QWidget *parent );

        /** Set the selection: prepare for showing the menu */
        void setSelection( QModelIndexList );

        /** Reset the menu */
        void reset();

        /** Over-ride close() to reset the context-menu. */
        Q_SLOT void close();

    private:
        QLabel *titleIcon = nullptr;
        QLabel *titleText = nullptr;

        /** For renames */
        QLineEdit *textLE = nullptr;

        /** To show contextual info */
        QLabel *contextLbl = nullptr;

        int folderCount = -1;
        int filesCount  = -1;
        int otherCount  = -1;

        QString mPath;

        /** Prepare the UI */
        void createUI();

        /** Hide/show various actions */
        void updateActions();

    protected:
        void paintEvent( QPaintEvent *pEvent );
        void keyReleaseEvent( QKeyEvent *kEvent );

        void mouseMoveEvent( QMouseEvent *mEvent );
};
