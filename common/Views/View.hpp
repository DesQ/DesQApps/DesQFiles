/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "Model.hpp"

#include "ContextMenu.hpp"

class DesQNavButton;

namespace DesQ {
    namespace Files {
        class View;
        class SideView;
    }
}

class DesQ::Files::View : public QListView {
    Q_OBJECT

    public:
        View( QWidget *parent );
        void setModel( QAbstractItemModel * ) Q_DECL_OVERRIDE;

        void setIconSize( const QSize& );
        void setGridSize( const QSize& );           // Suitable GridSize automatically set

        void setIconView();
        void setTileView();

        QModelIndexList selectedIndexes() const;

        static QSize gridSize;
        static int extraSpace;

    private:
        int viewportWidth = 512;
        int mSpacing      = 10;
        DesQ::Files::Model *filesModel;

        QStringList ioArgs;

        QPoint dragStartPosition;

        DesQNavButton *navBtn;
        DesQ::Files::ContextMenu *menu;

    protected:
        void resizeEvent( QResizeEvent *rEvent ) override;
        void contextMenuEvent( QContextMenuEvent *cEvent ) override;

    public Q_SLOTS:
        void updateItem( QModelIndex idx );

    private Q_SLOTS:
        void copy();
        void move();
        void link();

    Q_SIGNALS:
        void load( QString );
};


class DesQ::Files::SideView : public QListView {
    Q_OBJECT;

    public:
        SideView( QWidget *parent );

        void highlight( QString );

    private:
        void prepareModel();

        QStandardItemModel *mdl = nullptr;

    Q_SIGNALS:
        void load( QString );
};
