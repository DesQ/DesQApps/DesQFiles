/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

/**
 * DesQ Files View Dialogs
 * We have a need for a very large number of dialogs. These are listed below.
 * 1. New File/Folder Dialog - Needs a LBL LV, LE, Checkbox (Open-after-create) + 2 Buttons
 * 2. Rename Dialog          - Needs a LBL, LE + 2 Buttons
 * 3. Copy/Move to           - Needs a LBL, LV, New Folder Btn, LE (Why?) + 2 Buttons
 * 4. Open with              - Needs a LBL, LV, LE, Checkbox (Make default) + 2 Buttons
 * 5. Open in                - Simple widget with two buttons (New Tab/New Window)
 * 6. Properties             - Needs a lot of things
 * 7. Share                  - Needs a LBL, LV + 2 Buttons
 * 8. Delete/Trash           - Needs a LBL, LV + 2 Buttons
 * 9. Replace/Rename         - Needs a LBL/Text browser, 5 buttons
 *
 * While these are called dialogs, we will build these as widgets.
 * These widgets will be shown with the folder view as the parent,
 * as a part of the parent.
 **/

#pragma once

#include "Global.hpp"

namespace DesQ {
    namespace Files {
        class Dialog;
    }
}

class DesQ::Files::Dialog : public QWidget {
    Q_OBJECT;

    public:

        /**
         * This is a standard dialog which comes with a simple titlebar,
         * an informational label, a list-view, an option to add an extra
         * widget, and a line-edit and two buttons (Okay and cancel).
         * While the list-view, and the line-edit are optional, all the
         * other widgets are fixed. Only their contents may be altered.
         * The "Dialog" is a widget that takes up the full size of the
         * parent - most of which is a semi-transparent black bg, with
         * the main content in the center.
         */
        Dialog( QWidget *parent );

        /** Show/Hide list-view */
        void showListView( bool );

        /** Show/Hide line-edit */
        void showLineEdit( bool );

        /** Add an extra widget */
        void setExtraWidget( QWidget * );

        /** Set a model to the list view */
        void setModel( QStandardItemModel * );

        /** Set the dialog title */
        void setTitle( QString );

        /** Set the dialog description */
        void setDescription( QString );

        /** Set info for dialog's line edit */
        void setEditInfo( QString );

        /** Set the place-holder */
        void setPlaceholderText( QString );

        /** To access the selected index/indices */
        QModelIndexList selectedIndexes();

    private:
        QListView *mView;
        QLineEdit *mEdit;

        QLabel *mTitle;
        QLabel *mInfo;
        QLabel *mEditInfo;

        QToolButton *okBtn, *cancelBtn;

        QWidget *mExtra;
        QWidget *mParent;
        QWidget *mDialog;

        QGridLayout *mMainLyt;
        bool mAccepted;

        void createUI();

    public Q_SLOTS:
        void show();
        int exec();

    protected:
        void keyPressEvent( QKeyEvent *kEvent );
        void paintEvent( QPaintEvent *pEvent );

        void mouseReleaseEvent( QMouseEvent *mEvent );

    Q_SIGNALS:
        void closed();
};
