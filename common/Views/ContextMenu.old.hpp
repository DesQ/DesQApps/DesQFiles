/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "Model.hpp"
#include "ViewWidgets.hpp"

namespace DesQ {
    namespace Files {
        class ContextMenu;
    }
}

class DesQ::Files::ContextMenu : public QWidget {
    Q_OBJECT

    public:
        ContextMenu( QWidget *parent );

        /** Single selection: One file, or folder or other node */
        void setSelection( QString node );

        /** Multiple selection: More than on file|folder|node selection */
        void setSelection( int folder, int files, int other );

        /** Clear the selection */
        void clearSelection();

        /** Over-ride close() to reset the context-menu. */
        Q_SLOT void close();

    private:
        QLabel *titleIcon = nullptr;
        QLabel *titleText = nullptr;

        ActionButton *openBtn    = nullptr;
        ActionButton *cutBtn     = nullptr;
        ActionButton *copyBtn    = nullptr;
        ActionButton *pasteBtn   = nullptr;
        ActionButton *linkBtn    = nullptr;
        ActionButton *renameBtn  = nullptr;
        ActionButton *termBtn    = nullptr;
        ActionButton *trashBtn   = nullptr;
        ActionButton *newItemBtn = nullptr;
        ActionButton *propsBtn   = nullptr;

        ActionButton *okBtn     = nullptr;
        ActionButton *cancelBtn = nullptr;

        /** For renames */
        QLineEdit *textLE = nullptr;

        /** To show contextual info */
        QLabel *contextLbl = nullptr;

        /** Secondary base widgets */
        QWidget *openBase  = nullptr;
        QWidget *cutBase   = nullptr;
        QWidget *copyBase  = nullptr;
        QWidget *linkBase  = nullptr;
        QWidget *termBase  = nullptr;
        QWidget *textBase  = nullptr;
        QWidget *trashBase = nullptr;
        QWidget *newBase   = nullptr;

        int folderCount = -1;
        int filesCount  = -1;
        int otherCount  = -1;

        QString mPath;

        /** Prepare the UI */
        void createUI();

        /** Hide/show various actions */
        void updateActions();

    protected:
        void paintEvent( QPaintEvent *pEvent );

        void mouseMoveEvent( QMouseEvent *mEvent );
};
