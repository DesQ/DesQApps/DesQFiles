/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

namespace DesQ {
    namespace Files {
        class Model;
    }
}

class Thumbnailer : public QThread {
    Q_OBJECT

    public:
        Thumbnailer( QObject *parent );

        /* We add files here, for which thumbnail generation is needed */
        void acquire( QString filename );

    protected:
        /* Loop to generate thumbnails one by one and intimate the model */
        void run();

    private:
        /* The actual thumbnail generation takes place here */
        bool getThumb( QString item );

        /* I don't trust Qthread::isRunning(), custom flag to indicate that */
        bool isActive;

        /* List of files whose thumbnails are to be generated. */
        QStringList fileList;

    Q_SIGNALS:
        /* Signal to inform the model about thumbnail generated */
        void updateItem( QString );
};

class InfoGatherer : public QThread {
    Q_OBJECT

    public:
        InfoGatherer( QObject *parent );

        /* We add files here, for which thumbnail generation is needed */
        void acquire( QFileInfo fileInfo );

    protected:
        /* Loop to generate thumbnails one by one and intimate the model */
        void run();

    private:
        /* The actual thumbnail generation takes place here */
        void getInfo( QString item );

        /* I don't trust Qthread::isRunning(), custom flag to indicate that */
        bool isActive;

        /* List of files whose info needs to be generated. */
        QFileInfoList fileList;

    Q_SIGNALS:
        /* Signal to inform the model about thumbnail generated */
        void updateItem( QString );
};

class DesQ::Files::Model : public QFileSystemModel {
    Q_OBJECT;

    public:
        Model( QWidget *parent );
        ~Model();

        /* We want to ensure thumbnailing is possible for image files */
        QVariant data( const QModelIndex& idx, int role = Qt::DisplayRole ) const Q_DECL_OVERRIDE;

        /* Reload the model, applying name filters, dir filters, etc */
        void reload();

        /* Drag and Drop */
        Qt::DropActions supportedDropActions() const Q_DECL_OVERRIDE;
        Qt::DropActions supportedDragActions() const Q_DECL_OVERRIDE;
        Qt::ItemFlags flags( const QModelIndex& index ) const Q_DECL_OVERRIDE;
        QStringList mimeTypes() const Q_DECL_OVERRIDE;

        /* When something is dragged */
        QMimeData * mimeData( const QModelIndexList& indexes ) const Q_DECL_OVERRIDE;

        /* When something is dropped */
        bool dropMimeData( const QMimeData *, Qt::DropAction, int, int, const QModelIndex& ) Q_DECL_OVERRIDE;

        /* Reimplement setRootPath(...) to clear old data */
        QModelIndex setRootPath( const QString& newPath );

        /* Back and forward */
        QString previousFolder();
        QString nextFolder();
        QString parentFolder();

        /** To toggle hidden files */
        void setShowHidden( bool );

        /* Place to store the image thumbnails */
        static QHash<QString, QIcon> iconMap;

        /* Place to store the node info (size for files, children for dirs) */
        static QHash<QString, int> sizeMap;

        /* Supported image formats */
        static QStringList supportedFormats;

    private:
        /* Hook to obtain image thumbnails */
        Thumbnailer *thumbnailer;
        InfoGatherer *infoWorker;

        QModelIndex mCurIdx;

        static QFontMetrics fm;

        QStringList oldRoots;
        int currentPos;

        bool lowMem;

        void emitNavSignals();

    public Q_SLOTS:
        /* Once a thumbnail is generated intimate the view about it */
        void updateItem( QString filename );

    private Q_SLOTS:
        /* INTERNAL: currentIndex for font rendering */
        void setCurrentIndex( QModelIndex );

    Q_SIGNALS:
        /* Signal to intimate the view about the index data change */
        void updateItem( QModelIndex );

        void backEnabled( bool );
        void forwardEnabled( bool );
        void upEnabled( bool );
};
