/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <fcntl.h>
#include <dirent.h>

// Local Headers
#include "Model.hpp"
#include "IOProcesses.hpp"
#include <desq/Utils.hpp>
#include <desq/desq-config.h>
#include <desqui/UiUtils.hpp>

/* ##### Thumbnailer Class
 * ==========================================================================================================
 */
Thumbnailer::Thumbnailer( QObject *parent ) : QThread( parent ) {
    fileList.clear();
    isActive = false;
}


void Thumbnailer::acquire( QString filename ) {
    if ( not fileList.contains( filename ) ) {
        fileList << filename;
    }

    if ( not isActive ) {
        start();
    }
}


void Thumbnailer::run() {
    while ( true ) {
        if ( fileList.count() ) {
            isActive = true;

            QString filename = fileList.takeFirst();

            if ( getThumb( filename ) ) {
                emit updateItem( filename );
            }
        }

        else {
            isActive = false;
            return;
        }
    }
}


bool Thumbnailer::getThumb( QString item ) {
    // Thumbnail image
    QImage  pic;
    QPixmap thumb( QSize( 128, 128 ) );

    thumb.fill( Qt::transparent );

    QImageReader picReader( item );

    pic = picReader.read();

    if ( pic.isNull() ) {
        qDebug() << "Bad image";
        return false;
    }

    pic = pic.scaled( 512, 512, Qt::KeepAspectRatio, Qt::FastTransformation );
    pic = pic.scaled( 128, 128, Qt::KeepAspectRatio, Qt::SmoothTransformation );

    QPainter painter( &thumb );

    painter.drawImage( QRectF( QPointF( (128 - pic.width() ) / 2, (128 - pic.height() ) / 2 ), QSizeF( pic.size() ) ), pic );
    painter.end();

    DesQ::Files::Model::iconMap.insert( item, thumb );
    return true;
}


/* ##### InfoGatherer Class
 * ==========================================================================================================
 */
InfoGatherer::InfoGatherer( QObject *parent ) : QThread( parent ) {
    fileList.clear();
    isActive = false;
}


void InfoGatherer::acquire( QFileInfo fileInfo ) {
    if ( not fileList.contains( fileInfo ) ) {
        fileList << fileInfo;
    }

    if ( not isActive ) {
        start();
    }
}


void InfoGatherer::run() {
    while ( true ) {
        if ( fileList.count() ) {
            isActive = true;

            QFileInfo info = fileList.takeFirst();

            if ( info.isDir() ) {
                getInfo( info.filePath() );
            }

            else if ( info.isFile() ) {
                DesQ::Files::Model::sizeMap.insert( info.filePath(), info.size() );
            }

            else {
                DesQ::Files::Model::sizeMap.insert( info.filePath(), 0 );
            }

            emit updateItem( info.filePath() );
        }

        else {
            isActive = false;
            return;
        }
    }
}


void InfoGatherer::getInfo( QString item ) {
    struct dirent **namelist;
    int           n = scandir( item.toLocal8Bit().constData(), &namelist, NULL, NULL );

    /* We don't care about errors: If we can't read a folder, then it has 0 items */
    DesQ::Files::Model::sizeMap.insert( item, (n >= 0 ? n : 0) );
}


/* ##### File System Model
 * ==========================================================================================================
 */

QStringList           DesQ::Files::Model::supportedFormats = QStringList();
QHash<QString, QIcon> DesQ::Files::Model::iconMap          = QHash<QString, QIcon>();
QHash<QString, int>   DesQ::Files::Model::sizeMap          = QHash<QString, int>();

DesQ::Files::Model::Model( QWidget *parent ) : QFileSystemModel( parent ) {
    Q_FOREACH ( QByteArray imgFmt, QImageReader::supportedImageFormats() ) {
        supportedFormats << QString::fromLatin1( imgFmt ).toLower();
    }

    setFilter( QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs );

    if ( filesSett->value( "ShowHidden" ) ) {
        setFilter( filter() | QDir::Hidden );
    }

    thumbnailer = new Thumbnailer( this );
    connect( thumbnailer, SIGNAL(updateItem(QString)),     this, SLOT( updateItem( QString ) ) );

    infoWorker = new InfoGatherer( this );
    connect( infoWorker,  SIGNAL( updateItem( QString ) ), this, SLOT( updateItem( QString ) ) );

    currentPos = -1;

    DFL::Settings *shellSett = DesQ::Utils::initializeDesQSettings( "Shell", "Shell" );

    lowMem = shellSett->value( "LowMem" );
}


DesQ::Files::Model::~Model() {
    thumbnailer->terminate();
    thumbnailer->deleteLater();
}


QVariant DesQ::Files::Model::data( const QModelIndex& idx, int role ) const {
    QFileInfo info = fileInfo( idx );
    QString   fn   = info.fileName();
    QString   fp   = info.filePath();

    switch ( role ) {
        case Qt::DecorationRole: {
            if ( idx.column() != 0 ) {
                return QIcon();
            }

            /* We're showing a folder icon */
            if ( info.isDir() ) {
                QIcon folder = QIcon::fromTheme( "folder" );

                /* Home folder */
                if ( info.canonicalFilePath() == QDir::homePath() ) {
                    return QIcon::fromTheme( "user-home", folder );
                }

                else if ( fn.toLower() == "desktop" ) {
                    return QIcon::fromTheme( "user-desktop", folder );
                }

                else if ( fn.toLower() == "documents" ) {
                    return QIcon::fromTheme( "folder-documents", folder );
                }

                else if ( fn.toLower() == "downloads" ) {
                    return QIcon::fromTheme( "folder-downloads", folder );
                }

                else if ( fn.toLower() == "music" ) {
                    return QIcon::fromTheme( "folder-music", folder );
                }

                else if ( fn.toLower() == "pictures" ) {
                    return QIcon::fromTheme( "folder-pictures", folder );
                }

                else if ( fn.toLower().startsWith( "images" ) ) {
                    return QIcon::fromTheme( "folder-pictures", folder );
                }

                else if ( fn.toLower().startsWith( "dcim" ) ) {
                    return QIcon::fromTheme( "folder-pictures", folder );
                }

                else if ( fn.toLower() == "public" ) {
                    return QIcon::fromTheme( "folder-public", folder );
                }

                else if ( fn.toLower() == "templates" ) {
                    return QIcon::fromTheme( "folder-templates", folder );
                }

                else if ( fn.toLower() == "videos" ) {
                    return QIcon::fromTheme( "folder-videos", folder );
                }

                return folder;
            }

            /* Other nodes */
            else if ( filesSett->value( "ShowThumb" ) ) {
                if ( supportedFormats.contains( fn.split( "." ).last().toLower() ) ) {
                    /* If thumbnail is not acquired, acquire it */
                    if ( not iconMap.contains( fp ) ) {
                        thumbnailer->acquire( fp );
                    }

                    /* Acquired thumbnail */
                    else {
                        return iconMap.value( fp );
                    }
                }
            }

            /* For all other files */
            return DesQUI::Utils::getMimeIcon( fp );
        }

        case Qt::ForegroundRole: {
            QPalette pltt = qApp->palette();

            if ( info.isSymLink() ) {
                return pltt.link();
            }

            if ( info.isDir() and not info.isExecutable() ) {
                return pltt.windowText();
            }

            if ( not info.isReadable() ) {
                return QBrush( Qt::darkRed );
            }

            if ( info.isFile() and info.isExecutable() ) {
                return QBrush( Qt::darkGreen );
            }

            return pltt.color( QPalette::WindowText );
        }

        case Qt::FontRole: {
            QFont font( QFileSystemModel::data( idx, role ).value<QFont>() );

            if ( info.isSymLink() ) {
                return QFont( font.family(), font.pointSize(), font.weight(), true );
            }

            return font;
        }

        case Qt::ToolTipRole: {
            return info.fileName();
        }

        /* Used for number of nodes in a folder */
        case Qt::UserRole + 1: {
            QString size;

            if ( not sizeMap.contains( fp ) ) {
                infoWorker->acquire( info );

                if ( info.isDir() ) {
                    size = "0 items";
                }

                else {
                    size = "0 bytes";
                }
            }

            /* Acquired info */
            else {
                if ( info.isDir() ) {
                    size = QString( "%1 items" ).arg( sizeMap.value( fp ) );
                }

                else {
                    size = DesQ::Utils::formatSize( sizeMap.value( fp ) );
                }
            }

            return size;
        }

        /* Used for number of nodes in a folder */
        case Qt::UserRole + 2: {
            if ( info.isSymLink() ) {
                return 1;
            }

            if ( info.isFile() and info.isExecutable() ) {
                return 2;
            }

            return 0;
        }

        default: {
            return QFileSystemModel::data( idx, role );
        }
    }
}


void DesQ::Files::Model::reload() {
    setFilter( QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs );

    if ( filesSett->value( "ShowHidden" ) ) {
        setFilter( filter() | QDir::Hidden );
    }

    QFileSystemModel::setRootPath( rootPath() );
}


Qt::DropActions DesQ::Files::Model::supportedDragActions() const {
    return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}


Qt::DropActions DesQ::Files::Model::supportedDropActions() const {
    return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}


Qt::ItemFlags DesQ::Files::Model::flags( const QModelIndex& idx ) const {
    Qt::ItemFlags itemFlags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    QFileInfo     finfo     = fileInfo( idx );

    if ( finfo.isReadable() ) {
        if ( finfo.isDir() and finfo.isExecutable() ) {
            itemFlags |= Qt::ItemIsDragEnabled;
        }

        else if ( finfo.isFile() ) {
            itemFlags |= Qt::ItemIsDragEnabled;
        }
    }

    if ( finfo.isDir() and finfo.isWritable() and finfo.isExecutable() ) {
        itemFlags |= Qt::ItemIsDropEnabled;
    }

    return itemFlags;
}


QStringList DesQ::Files::Model::mimeTypes() const {
    QStringList types;

    types << "text/uri-list";
    return types;
}


QMimeData * DesQ::Files::Model::mimeData( const QModelIndexList& indexes ) const {
    QMimeData *data = new QMimeData();

    QList<QUrl> files;

    Q_FOREACH ( QModelIndex index, indexes ) {
        files << QUrl::fromLocalFile( filePath( index ) );
    }

    data->setUrls( files );

    return data;
}


bool DesQ::Files::Model::dropMimeData( const QMimeData *data, Qt::DropAction action, int, int, const QModelIndex& parent ) {
    if ( not data->hasUrls() ) {
        qDebug() << data;
        return false;
    }

    if ( not parent.isValid() ) {
        qDebug() << filePath( parent );
        return false;
    }

    if ( action == Qt::IgnoreAction ) {
        qDebug() << "Ignoring drop";
        return false;
    }

    QList<QUrl> urls = data->urls();
    QStringList srcList;

    IOProcess::Progress *progress = new IOProcess::Progress();

    /* No source dir. All the urls will be absolute */
    progress->sourceDir  = DesQ::Utils::dirName( urls.value( 0 ).toLocalFile() );
    progress->sourceDir += (progress->sourceDir.endsWith( "/" ) ? "" : "/");
    Q_FOREACH ( QUrl src, urls ) {
        if ( rootPath() + "/" != progress->sourceDir ) {
            srcList << src.toLocalFile().replace( progress->sourceDir, "" );
        }
    }

    if ( not srcList.count() ) {
        return false;
    }

    progress->targetDir = filePath( parent );

    switch ( action ) {
        case Qt::CopyAction: {
            progress->type = IOProcess::Type::Copy;
            break;
        }

        case Qt::MoveAction: {
            progress->type = IOProcess::Type::Move;
            break;
        }

        case Qt::LinkAction: {
            Q_FOREACH ( QString source, srcList ) {
                symlink( source.toLocal8Bit().constData(), rootDirectory().filePath( DesQ::Utils::baseName( source ) ).toLocal8Bit().constData() );
            }

            return true;
        }

        default: {
            return false;
        }
    }

    IODialog *pasteDlg = new IODialog( srcList, progress );

    pasteDlg->show();

    return true;
}


QModelIndex DesQ::Files::Model::setRootPath( const QString& newPath ) {
    /* Low resource mode */
    if ( lowMem ) {
        iconMap.clear();
        sizeMap.clear();
    }

    int tmpBackPos = (currentPos == 0 ? 0 : currentPos - 1);
    int tmpNextPos = (currentPos == oldRoots.count() - 1 ? currentPos : currentPos + 1);

    /** We're simply moving back */
    if ( oldRoots.length() && (oldRoots.at( tmpBackPos ) == newPath) ) {
        currentPos = tmpBackPos;
    }

    /** We're simply moving forward */
    else if ( oldRoots.length() && (oldRoots.at( tmpNextPos ) == newPath) ) {
        currentPos = tmpNextPos;
    }

    /** We're simply moving forward */
    else if ( oldRoots.length() && (oldRoots.at( currentPos ) == newPath) ) {
        // Nothing needs to be done. But we need this clause.
    }

    /** Something else */
    else {
        oldRoots.erase( oldRoots.begin() + currentPos + 1, oldRoots.end() );

        if ( oldRoots.count() ) {
            if ( oldRoots.last() != newPath ) {
                oldRoots << newPath;
            }
        }

        else {
            oldRoots << newPath;
        }

        currentPos = oldRoots.count() - 1;
    }

    emitNavSignals();

    return QFileSystemModel::setRootPath( newPath );
}


QString DesQ::Files::Model::previousFolder() {
    int pos = (currentPos == 0 ? 0 : currentPos - 1);

    return oldRoots.at( pos );
}


QString DesQ::Files::Model::nextFolder() {
    int pos = (currentPos == oldRoots.count() - 1 ? currentPos : currentPos + 1);

    return oldRoots.at( pos );
}


QString DesQ::Files::Model::parentFolder() {
    QFileInfo info( rootPath() );

    return info.path();
}


void DesQ::Files::Model::setShowHidden( bool shown ) {
    if ( shown ) {
        setFilter( QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Hidden );
    }

    else {
        setFilter( QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs );
    }
}


void DesQ::Files::Model::emitNavSignals() {
    /* currentPos >0 => can go back */
    if ( currentPos > 0 ) {
        emit backEnabled( true );
    }

    else {
        emit backEnabled( false );
    }

    /* currentPos not the last item in oldRoots */
    if ( currentPos < oldRoots.count() - 1 ) {
        emit forwardEnabled( true );
    }

    else {
        emit forwardEnabled( false );
    }

    /* If we're at root, we can't go up. */
    if ( QFileInfo( rootPath() ).isRoot() ) {
        emit upEnabled( false );
    }

    else {
        emit upEnabled( true );
    }
}


void DesQ::Files::Model::updateItem( QString filename ) {
    QModelIndex idx = QFileSystemModel::index( filename );
    emit        updateItem( idx );
}


void DesQ::Files::Model::setCurrentIndex( QModelIndex idx ) {
    mCurIdx = idx;
}
