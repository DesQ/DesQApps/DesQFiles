/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This file was orignially a part of libcprime.
 * (https://gitlab.com/cubocore/libcprime).
 * Suitable modifications have been done to suit the needs of this project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QIcon>
#include <QMessageBox>

class TrashManager {
    public:
        static QString trashLocation( const QString& path );
        static bool moveToTrash( const QStringList& filePaths );
        static void restoreFromTrash( const QStringList& filePaths );
        static bool deleteFileTotally( const QStringList& filePaths, bool fromTrash );

        static QString homePartition;

    private:
        static int showMessage( QMessageBox::Icon icon, const QString& title, const QString& message, QMessageBox::StandardButtons buttons );

        void deleteNode( QString path );
};
