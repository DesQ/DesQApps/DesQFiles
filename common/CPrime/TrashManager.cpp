/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This file was orignially a part of libcprime.
 * (https://gitlab.com/cubocore/libcprime).
 * Suitable modifications have been done to suit the needs of this project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QIcon>
#include <QDirIterator>
#include <QStorageInfo>
#include <QSettings>
#include <QInputDialog>
#include <QLineEdit>

#include <fcntl.h>
#include <unistd.h>

#include "TrashManager.hpp"
#include "IOProcesses.hpp"
#include <desq/Utils.hpp>

QString TrashManager::homePartition = QStorageInfo( QDir::homePath() ).rootPath();

/**
 * freedesktop.org says, check if home and path are on the same partition.
 * The $XDG_DATA_DIR/.Trash is the trash location.
 *
 * If not, check $mountpoint/.Trash/ if it exists, then ( create and ) use $mountpoint/.Trash/$uid
 * $mountpoint/.Trash/ should have sticky bit set ( S_ISVTX ), and should not be a symbolic link
 * readLink( $mountpoint/.Trash/ ) == $mountpoint/.Trash/
 * Otherwise, use $mountpoint/.Trash-$uid
 **/
QString TrashManager::trashLocation( const QString& path ) {
    /* Check if $HOME and @path are on the same partition, and path begins with $HOME */
    if ( (homePartition == QStorageInfo( path ).rootPath() ) and path.startsWith( QDir::homePath() ) ) {
        // Same partition, ensure all the paths exist
        QDir::home().mkpath( ".local/share/Trash/" );
        QDir::home().mkpath( ".local/share/Trash/files/" );
        QDir::home().mkpath( ".local/share/Trash/info/" );

        QFile::setPermissions( QDir::home().filePath( "/.local/share/Trash" ),       QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );
        QFile::setPermissions( QDir::home().filePath( "/.local/share/Trash/files" ), QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );
        QFile::setPermissions( QDir::home().filePath( "/.local/share/Trash/info" ),  QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );

        return QDir::home().filePath( ".local/share/Trash" );
    }

    else {
        QString mountPoint = QStorageInfo( path ).rootPath();

        /**
         * If the mount point does not exist, or we have read write issues return a NULL string
         */
        if ( access( mountPoint.toLocal8Bit().data(), R_OK | W_OK | X_OK ) ) {
            return QString();
        }

        /**
         * If $MNTPT/.Trash/$UID is present, and accessible with right permissions
         * We blindly try to make $MTPT/.Trash/$uid/files, $MTPT/.Trash/$uid/info
         */
        if ( access( (mountPoint + "/.Trash/" + QString::number( getuid() ) ).toLocal8Bit().data(), R_OK | W_OK | X_OK ) == 0 ) {
            QDir( mountPoint ).mkpath( QString( ".Trash/%1/files/" ).arg( getuid() ) );
            QDir( mountPoint ).mkpath( QString( ".Trash/%1/info/" ).arg( getuid() ) );

            /* Check if the any one above folders exist, say $MTPT/.Trash-$uid/files */
            if ( access( (mountPoint + "/.Trash/" + QString::number( getuid() ) + "/files/").toLocal8Bit().data(), R_OK | W_OK | X_OK ) == 0 ) {
                return mountPoint + "/.Trash/" + QString::number( getuid() );
            }
        }

        /* Otherwise we create $MNTPT/.Trash-$UID */
        QDir( mountPoint ).mkpath( QString( ".Trash-%1/" ).arg( getuid() ) );
        QDir( mountPoint ).mkpath( QString( ".Trash-%1/files/" ).arg( getuid() ) );
        QDir( mountPoint ).mkpath( QString( ".Trash-%1/info/" ).arg( getuid() ) );
        QFile::setPermissions( mountPoint + "/.Trash-" + QString::number( getuid() ), QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner );

        /* Check if the any one above folders exist, say $MTPT/.Trash-$uid/files */
        if ( access( (mountPoint + "/.Trash-" + QString::number( getuid() ) + "/files/").toLocal8Bit().data(), R_OK | W_OK | X_OK ) == 0 ) {
            return mountPoint + "/.Trash-" + QString::number( getuid() );
        }

        return QString();
    }
}


bool TrashManager::moveToTrash( const QStringList& filePaths ) {
    QStringList toBeTrashed( filePaths );
    QStringList failedList;

    failedList.clear();
    int count = 0;
    int done  = 0;

    QStringList toBeDeleted;

    for ( QString filePath: filePaths) {
        QFileInfo info( filePath );

        if ( info.size() >= (1 << 30) ) {
            toBeDeleted << filePath;
        }
    }

    if ( toBeDeleted.count() ) {
        QString text(
            "<p>One or more files you're sending to trash is larger than 1 GiB."
            "Sending these file(s) will not free up disk space.</p>"
            "<p>Do you want to delete it instead?</p>"
        );

        int reply = QMessageBox::question(
            nullptr,
            "DesQ Files | Large file(s)",
            text,
            QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes
        );

        if ( reply == QMessageBox::Yes ) {
            deleteFileTotally( toBeDeleted, false );
            for ( QString path: toBeDeleted ) {
                toBeTrashed.removeAll( path );
            }
        }
    }

    for ( QString filePath: toBeTrashed ) {
        QFileInfo info( filePath );
        QString   trashLoc = trashLocation( info.filePath() );

        if ( !trashLoc.count() ) {
            // Failed to move to trash
            failedList[ count++ ] = info.filePath();
            continue;
        }

        QString newPath = QDir( trashLoc ).filePath( "files/" + info.fileName() );
        QString delTime = QDateTime::currentDateTime().toString( "yyyy-MM-ddThh:mm:ss" );

        // If it exists, add a date time to it to make it unique
        if ( QFile::exists( newPath ) ) {
            newPath += delTime;
        }

        /* Try trashing it. If it fails, intimate the user */
        if ( not QFile::rename( info.filePath(), newPath ) ) {
            failedList << info.filePath();
        }

        /* If it succeeds, we write the meta data */
        else {
            QFile metadata( trashLoc + "/info/" + DesQ::Utils::baseName( newPath ) + ".trashinfo" );
            metadata.open( QIODevice::WriteOnly );
            metadata.write(
                QString(
                    "[Trash Info]\n"
                    "Path=%1\n"
                    "DeletionDate=%2\n"
                ).arg( info.filePath() ).arg( delTime ).toLocal8Bit()
            );
            metadata.close();
            done++;
        }
    }

    bool    ret = true;
    QString msg = "";

    if ( failedList.count() ) {
        msg = "Some items could not be deleted. Please check if you have the right permissions, and try again.";
        ret = false;
    }
    else {
        if ( done ) {
            msg = "Trashing of the selected items is complete.";
            ret = true;
        }
        else {
            msg = "Nothing trashed.";
            ret = false;
        }
    }

    return ret;
}


/* Since we support multiple trash locations, use absolute trash paths: ~/.local/share/Trash/files/path */
void TrashManager::restoreFromTrash( const QStringList& filePaths ) {
    int failed   = 0;
    int response = -1;

    Q_FOREACH (const QString& fileToRestore, filePaths) {
        QDir trash = QDir( trashLocation( fileToRestore ) );

        // Read trashinfo
        QSettings trashInfo( trash.path() + "/info/" + DesQ::Utils::baseName( fileToRestore ) + ".trashinfo", QSettings::IniFormat );

        if ( trashInfo.contains( "Trash Info/Path" ) ) {
            QString origPath = trashInfo.value( "Trash Info/Path" ).toString();

            if ( DesQ::Utils::exists( origPath ) ) {
                QString origLoc  = DesQ::Utils::dirName( origPath );
                QString origName = DesQ::Utils::baseName( origPath );

                /* If the user had previously said yes or no, then ask again */
                if ( response & (QMessageBox::Yes | QMessageBox::No | QMessageBox::Ignore) ) {
                    response = QMessageBox::question(
                        nullptr,
                        "Replace existing file?",
                        QString(
                            "<p>The file <b><tt>%1</tt></b> you are trying to restore from trash exists in </p>"
                            "<p><center><b><tt>%2</tt></b></center></p>"
                            "<p>Do you want to replace it?</p>"
                            "<tt>[Yes]</tt> - Restore and replace<br>"
                            "<tt>[Yes to All]</tt> - Restore and replace all existing files<br>"
                            "<tt>[No]</tt> - Restore and keep both files<br>"
                            "<tt>[No to All]</tt> - Restore and keep all existing files<br>"
                            "<tt>[Ignore]</tt> - Do not restore<br>"
                        ).arg( origName ).arg( origLoc ),
                        QMessageBox::No | QMessageBox::NoToAll | QMessageBox::Yes | QMessageBox::YesToAll | QMessageBox::Ignore
                    );
                }

                /* If the user had said yes/yes to all, delete the existing file and then continue */
                if ( response & (QMessageBox::Yes | QMessageBox::YesToAll) ) {
                    if ( DesQ::Utils::isDir( origPath ) ) {
                        DesQ::Utils::removeDir( origPath );
                    }

                    else {
                        QFile::remove( origPath );
                    }
                }

                /* If the user had said no/no to all, ask for a new name and then continue */
                else if ( response & (QMessageBox::No | QMessageBox::NoToAll) ) {
                    bool ok = false;
                    while ( DesQ::Utils::exists( origPath ) ) {
                        QString fn = QInputDialog::getText(
                            nullptr,
                            "Input new file name", QString(
                                "<p>The file <b><tt>%1</tt></b> already exists in</p>"
                                "<p><center><b><tt>%2</tt></b></center></p>"
                                "Please enter a new name:"
                            ).arg( origName ).arg( origLoc ),
                            QLineEdit::Normal,
                            origName,
                            &ok
                        );

                        if ( not ok ) {
                            break;
                        }

                        origPath = origLoc + fn;
                    }

                    /* If the user does not provide a new name */
                    if ( not ok ) {
                        continue;
                    }
                }

                /* Ignore the restoration of this file */
                else {
                    failed++;
                    continue;
                }
            }

            /* Failed to move it to original path */
            if ( not QFile::rename( fileToRestore, origPath ) ) {
                failed++;
            }

            /* Success! Remove the info file */
            else {
                QFile::remove( trashInfo.fileName() );
            }
        }

        else {
            qDebug() << "Problematic info file:" << trashInfo.fileName();
            failed++;
        }
    }
}


bool TrashManager::deleteFileTotally( const QStringList& filePaths, bool fromTrash ) {
    QStringList failedList;

    Q_FOREACH (const QString& filePath, filePaths) {
        QFileInfo info( filePath );

        QString trashLoc = TrashManager::trashLocation( filePath );

        // Check whether the directory is writable
        if ( not DesQ::Utils::isWritable( DesQ::Utils::dirName( filePath ) ) ) {
            failedList << filePath;
            continue;
        }

        // Check whether the file is link
        if ( info.isSymLink() ) {
            if ( !QFile::remove( filePath ) ) {
                failedList << filePath;
                continue;
            }
        }

        else {
            if ( DesQ::Utils::isDir( filePath ) ) {
                if ( not DesQ::Utils::removeDir( filePath ) ) {
                    failedList << filePath;
                }
            }

            else {
                if ( not QFile::remove( filePath ) ) {
                    failedList << filePath;
                }
            }

            // ========

            // Check is delete done for emptying trash
            if ( fromTrash ) {
                // Delete trash info
                QFile info( trashLoc + "/info/" + DesQ::Utils::baseName( filePath ) + ".trashinfo" );

                if ( info.exists() ) {
                    info.remove();
                }

                else {
                    qDebug() << "Failed to remove info:" << info.fileName();
                }
            }
        }
    }

    bool ok = true;

    if ( failedList.count() ) {
        ok = false;
    }
    else {
        qDebug() << "Finished";
    }

    return ok;
}
