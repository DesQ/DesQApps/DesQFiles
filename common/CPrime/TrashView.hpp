/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This file was orignially a part of libcprime.
 * (https://gitlab.com/cubocore/libcprime).
 * Suitable modifications have been done to suit the needs of this project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "TrashModel.hpp"

class TrashView : public QTableView {
    Q_OBJECT

    public:
        TrashView( QWidget *parent = nullptr );

    private:
        CTrashModel *tModel;
        QAction *restoreAct, *deleteAct;
        QAction *refreshTrashAct;

        QToolButton *btn1, *btn2;

    public Q_SLOTS:
        void refresh();

        void restoreTrashed();
        void deleteTrashed();

    private Q_SLOTS:
        void contextMenu( const QPoint& );
};
